#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

XD=`date`
echo "Start indexing at $XD";

# pass argument "index" if you want to create only index.csv (and no SQL)
if [[ -z $1 ]];
then 
    echo "No parameter passed."
else
   ONLYINDEX=$1
   echo "Parameter passed = $1"
fi

# lock the process..
PIDFILE="$SCRIPT_DIR/tmp/doindex.lock"
remove_pidfile()
{
  rm -f "$PIDFILE"
}
another_instance()
{
  echo "There is another instance running, exiting"
  exit 1
}
if [ -f "$PIDFILE" ]; then
  kill -0 "$(cat $PIDFILE)" && another_instance
fi
trap remove_pidfile EXIT
echo $$ > "$PIDFILE"


# echo "Indexing repository:"

ZWI_REPO=$SCRIPT_DIR/ZWI
echo "Indexing repository:$ZWI_REPO"

if [ -z "$USER" ]
then
   echo "The user $USER is PHP"
   echo "Use $ZWI_REPO"

   if [[ -z $1 ]];
   then 
           echo "Make index.csv.gz and SQL"
	   python3 $SCRIPT_DIR/ZWINetwork/make_index.py -i $SCRIPT_DIR/ZWI
           python3 $SCRIPT_DIR/zwi_sqlite.py
	   date
	   exit 1
   else
          echo "Make index.csv.gz only"
          python3 $SCRIPT_DIR/ZWINetwork/make_index.py -i $SCRIPT_DIR/ZWI
	  date
          exit 1

   fi


else
 if [ -w "$ZWI_REPO" ] 
 then
   echo "Use $ZWI_REPO"
   echo "The user $USER is a person"
   if [[ -z $1 ]];
   then
           echo "Make index.csv.gz and SQL"
	   python3 $SCRIPT_DIR/ZWINetwork/make_index.py -i $SCRIPT_DIR/ZWI
           python3 $SCRIPT_DIR/zwi_sqlite.py
	   date
           exit 1
   else
          echo "Make index.csv.gz only"
	  python3 $SCRIPT_DIR/ZWINetwork/make_index.py -i $SCRIPT_DIR/ZWI
          date
         exit 1

   fi

  else
   echo "Write permission is NOT granted on $ZWI_REPO"
  fi

fi 
