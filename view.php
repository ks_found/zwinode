<?php

require_once("config.php");
require_once("comstyle.php");
require_once("common.php");

#$id=-1;
#if (isset($_GET['id'])) $id = $_GET['id'];
#if ($id<0) die("Wrong index of the article");
#if (isInteger($id) == false)
#            die("ID of the article id not integer");
$id="0";
if (isset($_GET['id'])) $id = $_GET['id'];
if (strlen($id) != 12) die("ID of the article is not found");

$action="";
if (isset($_GET['action'])) $action = $_GET['action'];


$db = null;
try {
   $db = new PDO($databasefile);
} catch( PDOException $e ) {
   die( $e->getMessage() );
}

// if directory does not exists, make it
if (!file_exists('tmp')) {
                         mkdir('tmp', 0777, true);
}


$sql = "SELECT path,title,publisher FROM " . $sTable . " WHERE hash='$id'" . " LIMIT 1;";
if ($sth = $db->prepare($sql)) {
           $sth->execute();
}

# $result = $db->query($sql);
$rowarray = $sth->fetchall(PDO::FETCH_ASSOC);
$rowno = 0;

//die("OK");

$title="";
$publisher="";
$zfile="";
foreach($rowarray as $row) {
if (isset($row['path']))
        $zfile=$row['path'];

if (isset($row['title']))
        $title=$row['title'];

if (isset($row['publisher']))
        $publisher=$row['publisher'];

}


$title=str_replace("_", " ", $title);


$full_path=$BASE . "/" . $zfile . ".zwi";
// use this action for ZWI path export
if ($action=="zwi"){
       $furl = str_replace("#", "%23", $zfile);
       $url_zwi= baseURL() . "/ZWI/en/". $furl . ".zwi";
       print($url_zwi);
       die();
}

$random_name=$id; // random_string(12);

$tmp_dir="tmp/" . $random_name;

$isCached="";
// if exists, but old directory, make it again
if (file_exists($tmp_dir. "/article.html") == true) {
  $getLastModZWI = filemtime($full_path);
  $getLastModHTML = filemtime($tmp_dir);
  //die($getLastModZWI . " " . $getLastModHTML);
  //ZWI file should be newer
  if ($getLastModHTML <  $getLastModZWI+1) {
   $cmd1=" rm -rf " .  $tmp_dir . ";";
   $cmd2=" unzip '" . $full_path . "' -d " . $tmp_dir;
   $output = shell_exec( $cmd1 . $cmd2  );
   $output = shell_exec( "chmod -R 755 " . $tmp_dir  );
  } else {
   $isCached=" <i>Cached article</i>";
  }

} else { // if directory does not exists


if (!file_exists( $tmp_dir )) {
  if (!mkdir($tmp_dir, 0777, true)) {
   die('Failed to create directory. Full disk?');
   }
}


$cmd=" unzip '" . $full_path . "' -d " . $tmp_dir;
//echo $cmd;
$output = shell_exec( $cmd );
$output = shell_exec( "chmod -R 755 " . $tmp_dir  );
};


// process json
$meta_s = file_get_contents( $tmp_dir . "/metadata.json" );
if ($meta_s === null) {
    die("Metadata file not fond for this ZWI");
}


$ZWImeta=false;
$html_file=$tmp_dir . "/article.html";
if ( file_exists( $html_file ) == false) {
         $ZWImeta=true;	
	 };


$meta = json_decode($meta_s, true);

$sourceurl="No source URL found";
if( isset( $meta['SourceURL'] ) ){
   $sourceurl=$meta['SourceURL'];
}

// redirect for META ZWI
if ($ZWImeta == true && $sourceurl != "No source URL found"){
	header("Location: " . $sourceurl);
        die();
}	


$licence_txt="No license is given";
if( isset( $meta['License'] ) ){
   $licence_txt=$meta['License'];
}


$categories=array();
if( isset( $meta['Categories'] ) ){
   $categories=$meta['Categories'];
}

$categories_txt="";
foreach($categories as $item) {
     $categories_txt = $categories_txt . " [" . $item .  "]";
}

$ff=footer();
$strFooter = <<<EOD
<footer class="bg-white text-center text-black">
    <p>$ff</p>
</footer>
EOD;

$DOWNLOAD_ZWI="&#8615; <a href=\"zwiget.php?id=".$id."\">  Download as ZWI file </a>";

#$start=file_get_contents("./html_start.html");
#print($start);

$sitename=$conf['title'];

$s1=<<<EOD

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="index.php">$sitename</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

        <li class="nav-item">
          <a class="nav-link" href="articles/">Articles</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="edit/">Edit</a>
        </li>


	<li class="nav-item">
          <a class="nav-link" href="about/">About</a>
        </li>


      </ul>

      <form class="d-flex" role="search" action="/find.php" method="get">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>


    </div>
  </div>
  <!-- Navbar content -->
</nav>

EOD;


$s2=<<<EOD
<style>
body {
  padding-top: 0px !important;
  padding-left: 10px !important;
}

h1 {
    margin-left: 12px !important;
}

</style>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.bundle.min.js"></script> 

<style>

.navbar-brand {
  color: #85c1e9;
  --bs-bg-opacity: 1;
  background-color: rgba(var(--bs-dark-rgb),var(--bs-bg-opacity)) !important;
}

body {
  margin: 0px;
  padding-left: 0px !important;
  font-size: 16px;
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
}


.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #85c1e9;
}

</style>

EOD;

$Views="";

if ($conf['search_in_descriptions'] == 1) {
//************************ page counter *****************************//
// logging page hits
$dbname = "viewlog.sqlite3";

$page=$id;

// check if database file exists first
if(!file_exists($dbname))
{
 $logdb = new PDO("sqlite:".$dbname);
 $logdb->exec("CREATE TABLE hits(page VARCHAR(255) PRIMARY KEY, title VARCHAR(255), publisher VARCHAR(255), counter INTEGER)");
}
else
{
 $logdb = new PDO("sqlite:".$dbname);
}

$counter=0;
$Views=$counter . " views";

// check if page is already in the hits table
$statement = $logdb->query("SELECT counter FROM hits WHERE page='$page'");
$record = $statement->fetchAll();

// if a record is found
if(sizeof($record) != 0)
{
        $counter = $record[0]['counter'];
        $counter = $counter+1;
        $logdb->exec("UPDATE hits SET counter=$counter WHERE page='$page'"); 
        $Views=$counter . " views";
}
else
{
 $logdb->exec("INSERT INTO hits(page, counter, title, publisher) VALUES ('$page', 1, '$title', '$publisher')");
 $counter=1;
 $Views=$counter . " views";
}

// close connection
$logdb = null;

};


$TXT_LICENSE="<hr><div style=\"margin-bottom:20px;margin-top:20px;margin-left:20px;\"> " . $DOWNLOAD_ZWI . "<br> &#9776; Source: <a href=\"" .  $sourceurl . "\">". $sourceurl . "</a> | License: "  . $licence_txt . " | " . $Views . "<br><br></div>";


// keep in memory for 2h
$TimeToKeepInMem=7200;
$MEM_KEY_EXT="zwinode:" . $random_name;   

// main article
$html_new = apcu_fetch( $MEM_KEY_EXT );
//$html_new = null; 
if ($html_new == null) {
$html=file_get_contents($tmp_dir . "/article.html"); 
$html=removeLink($html ); // remove URL links
$html_new=str_replace("data/media/images/", $tmp_dir . "/data/media/images/", $html);
$html_new=str_replace("data/css/", $tmp_dir . "/data/css/", $html_new);
$intro="<h1>" . $title . "</h1>\n <div style=\"margin:10px;color:#154360;font-size:20px;background-color:#dddddd;\">&nbsp;From " . ucwords($publisher) . "</div>\n";
# $html_new  = str_replace("<!-- Bootstrap core CSS -->", "<!-- Bootstrap core CSS -->\n" . $s2,  $html_new);
$html_new  = str_replace("</head>", $s2 . "\n</head>",  $html_new);
# $html_new  = str_replace("<!-- BEGIN BODY -->", "<!-- BEGIN BODY -->\n" . $s1 . $intro, $html_new);
$html_new  = str_replace("<body>", "<body>\n" . $s1 . $intro, $html_new);
$html_new  = str_replace("</body>", "\n" . $CATEGORIES . "\n" . $TXT_LICENSE . "\n". $isCached . "\n" .  $strFooter . "\n</body>", $html_new);
$html_new  = "<!DOCTYPE html>\n" . $html_new;
// store it
apcu_store($MEM_KEY_EXT, $html_new, $TimeToKeepInMem); 
} 

print($html_new);

$db = null;



?>


