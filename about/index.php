<!DOCTYPE html>
<?php
require_once("../comstyle.php");
?>

<html >
    <head>
       <title>About <?php echo $conf['title'];?></title>
       <meta charset="UTF-8">
       <meta name="description" content="<?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="KSF">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>

<style>
.navbar-brand {
  color: #85c1e9;
}

.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #85c1e9;
}

h1 {
font-size: 28px;
font-weight: 600;
line-height: 1.4;

}

h2 {
font-size: 24px;
font-weight: 600;
line-height: 1.4;
margin-top: 20px;
color: #6995b3; 
}

pre {
  font-size: 1em;
  border: 2px solid grey;
  width: 450px;
  border-left: 10px solid #6495ED;
  border-radius: 5px;
  padding: 14px;
}


</style>


    </head>
    <body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../"><?php echo $conf['title'];?></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

       <li class="nav-item">
          <a class="nav-link" href="../articles/">Articles</a>
        </li>

       <li class="nav-item">
          <a class="nav-link" href="../editor/">+Add</a>
        </li>

	 <li class="nav-item">
          <a class="nav-link" href="../edit/">Edit</a>
        </li>

	<li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search"> 
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">

<h2>About</h2>

ZWINode is a client for  the  decentralized network called <a href="https://encyclosphere.org/">Encyclosphere</a>  which allows:

<ul>

<li>
Maintain a local repository of ZWI files. 
</li>

<li>
Index this repository for local use and for download by external nodes. 
</li>

<li>
View locally stored ZWI articles.  
</li>

<li>
Add new articles to the local repository. 
</li>

<li>
Track the history of articles.
</li>


<li>
Create full replicas of other repositories with articles. 
</li>

</ul>

Use the <a href='../edit/'>maintenance page</a> to add more articles or start sharing the articles with 
similar Encyclosphere sites.  

<p>
</p>

<h2>Project page</h2>

Use this link to find the most recent <a href='https://gitlab.com/ks_found/zwinode'>ZWINode package</a>.

<p>
</p>

<a href='https://encyclosphere.org/about/'>
<i> - The Knowledge Standards Foundation - </i>
</a>

</div>


<footer class="bg-white text-center">
    <p><?php echo footer()  ?>   </p>
</footer>


<p>
</p>


</div>




</body>
</html>
