<?php

$query ="";
if (isset($_GET['query']))  $query =$_GET['query'];
$query=trim($query);
$query = str_replace('_', ' ', $query);
$query = str_replace('+', ' ', $query);

$start =0;
if (isset($_GET['start']))  $start =$_GET['start'];
$start=trim($start);

// if nolog=1, then we do not put it to logfile
$nolog =0;
if (isset($_GET['nolog']))  $nolog =$_GET['nolog'];
$nolog=trim($nolog);

//die($query);

// how many results per encyclopedia 
// do not change it since we also convert this to offset for other encyclopedias
$BATCH=100;

$fulltext=0; // all topics, but using article IDs 
if (isset($_GET['fulltext']))  $fulltext =$_GET['fulltext'];
$fulltext=trim($fulltext);

$checkContent="";
if ($fulltext==1) $checkContent="checked";
$checkTitle="";
if ($fulltext==0) $checkTitle="checked";

$query = preg_replace('/\s+/', ' ',$query); // multiple spaces 
$query_def=$query;

putenv('LANG=en_US.UTF-8');

function contains($needles, $haystack) {
    return count(array_intersect($needles, explode(" ", preg_replace("/[^A-Za-z0-9' -]/", "", $haystack))));
}


$array = array('porn', 'jerk', 'ass', 'shit', 'pussy', 'vagina', 'fuck','anal','blowjob','cock','anus','dick','jizz');
$i = contains($array, $query);
if ($i == True)  {$query="science";  
     print("<script> window.location = 'index.php'; </script>"); 
     }


echo <<<EOL
<!DOCTYPE html>
<html >
    <head>
       <title>Search results</title>
       <meta charset="UTF-8">
       <meta name="description" content="ZWINode publishing platform">
       <meta name="keywords" content="ZWI DB">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
       <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" type="text/css" href="css/style.css"/>

<style>
.navbar-brand {
  color: #85c1e9;
}
</style>


    </head>
    <body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="index.php">ZWINode</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">
        <li class="nav-item">
          <a class="nav-link" href="about.php">About</a>
        </li>
      </ul>

      <form class="d-flex" role="search" action="/find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>


    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">

EOL;


$URL="location.href='index.php';"; 
$extrabutton = <<<EOD
<div style="margin-top:50px; position: absolute; right: 10px; top: 15px; font-size:14px;">
<button class="hwbutton" onclick="$URL">Back</button>
</div>
EOD;

$extrabutton_bottom = <<<EOD
<div style="font-size:14px;">
<button class="hwbutton" onclick="$URL">Go Back</button>
</div>
EOD;


// next 
$start1=$start+$BATCH;
$NEW_URL1="location.href='find.php?query=" . $query . "&start=$start1';";
$newbutton1 = <<<EOD
<div style="padding-right:5px; margin-left:20px; display:inline-block; width:120px; font-size:14px;">
<button class="hwbutton" onclick="$NEW_URL1">Next >> </button>
</div>
EOD;

// previous
$start2=$start-$BATCH;
if ($start2<0) $start2=0;
$NEW_URL2="location.href='find.php?query=" . $query . "&start=$start2';";
$newbutton2 = <<<EOD
<div style="padding-right:5px; margin-left:20px; display:inline-block; width:120px;  font-size:14px;">
<button class="hwbutton" onclick="$NEW_URL2"> << </button>
</div>
EOD;

$NEW_URL3="location.href='find.php?query=" . $query . "&start=$start2&fulltext=1';";
$newbuttontext = <<<EOD
<div style="padding-left:5px; margin-left:20px; display:inline-block; width:320px;  font-size:16px;">
Nothing was found in titles. <button class="hwbutton" onclick="$NEW_URL3">Search in fulltext? </button>
</div>
EOD;


echo("<h3>&nbsp;Search results for: \"<i>$query_def</i>\"</h3>");

$xlength=strlen($query);


if ( ($xlength>2 && $xlength<80) ) {


// check cache
$apcuAvailabe = function_exists('apcu_enabled') && apcu_enabled();
if($apcuAvailabe == false) {
  die("Sorry, apcu is not availble. We need apcu cacher installed on this server");
}

# key for storage
$MEM_KEY_INT="zwinode:" .$start ." " . $BATCH ." ".$fulltext." ".$query . "_INT";

// keep in memory for 2h*12 
$TimeToKeepInMem=7200*6;

####################### Running Lucene ##########################################
#
// SQL database
require_once("config.php");
require_once("comstyle.php");
require_once("common.php");

$sWhere = "WHERE ( title LIKE '%". $query ."%')";
if ( $conf['search_in_descriptions'] == 1)  $sWhere = "WHERE ( title LIKE '%". $query ."%' OR description LIKE '%". $query ."%')";
if ($fulltext == 1) $sWhere = "WHERE ( description LIKE '%". $query ."%')";


$sOrder = "ORDER BY length(title) ASC";
$COMM="";
try {
    //open the database
    $db = new PDO($databasefile);
    $qq="SELECT * FROM ".$sTable . " " . $sWhere . " " . $sOrder . ";";
    $result = $db->query($qq);
    $rowarray = $result->fetchall(PDO::FETCH_ASSOC);
    $rowno = 0;
    foreach($rowarray as $row)
         {
           $title=$row["title"];
           $publisher=ucfirst($row["publisher"]);
           $hash=$row["hash"];
           $description = $row["description"];
           $xtime = date('m/d/Y', $row["timestamp"]);
           //print $rowno . "   " . $title . "  " . $publisher .  "<br>\n";

           $description = doHighlight($description , $query); 
	   $TXT="<li><span class=\"xitem\"><span class=\"x1\"> <a href=\"view.php?id=$hash\">$title</a></span>  <span class=\"x2\">(from $publisher)</span> : $description ... </span></span>  <span class=\"x3\">[$xtime]</span> </li>"; 
          $COMM = $COMM . $TXT;
	   $rowno++;
         }
    // close the database connection
    $db = NULL;
    } catch(PDOException $e) {
        print 'Exception : '.$e->getMessage();
    }

if ($COMM<10)
   echo $newbuttontext; 

$str1 = <<<EOD
<div class="container-fluid">
<div class="row">
<ol>
$COMM
</ol>
</div>
</div>
EOD;

$noShow=false;
if (strlen($COMM) < 20) {
  $pos1 = strpos($COMM, "Nothing was found");
  if ($pos1 !== false) {
   $noShow=true;
  }
} 

if ($noShow==false) 
	  print($str1);
else
	  print("<p>&nbsp;No results</p>");

# print($iscaching_int);
# print($iscached_ext);



};

if ($xlength>79){
  print "This string is too long to process!";
}

if ($xlength<=2) {
  print "This string is too short to process!";
};

// supress buttoms if no much of data
if (strlen($COMM) < 1000) { 
	$newbutton2="";
        $newbutton1="";
};


print "</div>";

$ff=footer();

$str = <<<EOD


</div>
	
$newbutton2   $newbutton1

<p></p>

<div style="margin-bottom: 1.6cm; margin-left:20px;"> 
$extrabutton_bottom
</div>

<footer class="bg-white text-center">
    <p>$ff</p>
</footer>

</div>


<script src="r/js/jquery.min.js?version=3.13.1"></script>
<script src="r/js/bootstrap.bundle.min.js?version=3.13.1"></script>
</body>
</html>

EOD;

echo $str;

?>

