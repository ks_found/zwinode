#!/bin/bash
# Task: clear the installation 

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
echo "Initializing repository:"

ZWI_REPO=$SCRIPT_DIR/ZWI

rm -rf tmp
rm -rf $SCRIPT_DIR/about/ZWI
rm -rf $SCRIPT_DIR/articles/ZWI
rm -rf $ZWI_REPO
rm -f *.log
rm -f $SCRIPT_DIR/edit/*.log
rm -f $SCRIPT_DIR/about/*.log

echo "All cleared"
