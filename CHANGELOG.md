# Change Log

All notable changes of ZWINode are documented in this file.

## [1.3] - 2024-02-11. Final version 
Main changes:

- Common styles for all footers 
- Footers are configurable 
- Multiple tests with > 3 million articles

## [1.3] - 2024-02-01. Beta 
Main changes:

- Support for DIFF and versioning
- Do not allow create publishers without domain names
- Correct styles  

## [1.2] - 2023-12-09. Alpha 
Main changes:

- Support for article editor (in progress)


## [1.1] - 2023-11-21. Final
Main changes:

- Support for showing the number of page view (at the bottom of articles) 
- Support for a page with 500 most popular ZWI files 
- Support for meta-data ZWI files 
- Fixed import from encyclosearch.org  
- Article listing panel indicates meta-only ZWI files with the letter "M"
- Additional options in config.php: (1) search in article descriptions; (2) page counter can be disabled for speed up.
 
## [1.1b] - 2023-11-16. Unreleased 

### Added
- Support for showing the number of page views
- Support for a page with 500 most popular ZWI files
 
## [1.1a] - 2023-11-05. Unreleased 
 
### Added
- Support for meta-data only ZWI files
- Article listing panel indicates meta-only ZWI files with the letter "M"
- Added the description of the article listing page
- Additional option in config.php to allow search in article descriptions and titles at the same time
- When searching in descriptions, found words are highlighted when $conf['search_in_descriptions'] = 1; in the config file)  

### Fixed
- Fixed a bug that prevented import of meta-data only ZWI files from encycloreader.
- Example: Try to add the URL link using "Edit" panel:
  https://encyclosearch.org/encyclosphere/database/en/encusindustries/www.referenceforbusiness.com/industries%23Agriculture-Forestry-Fishing%23Corn.html.zwi
 
