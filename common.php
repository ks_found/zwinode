<?php

        define('ROOTPATH', __DIR__);
        $BASE=ROOTPATH . "/ZWI/en/";

        // SQLite 
        $databasefile="sqlite:".$BASE . "index.sqlite";

	$aColumns = array( 'path','title','publisher','hash','filesize','timestamp','license','rating','description');
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "rowid";
        /* DB table to use */
        $sTable = "dbase_zwi";

	
// check cookies enables
function checkCookies() {
if (isset($_COOKIE['cookieCheck'])) {
    return true; 
} else {
    if (isset($_GET['reload'])) {
        return false;
    } else {
        setcookie('cookieCheck', '1', time() + 60);
        header('Location: ' . $_SERVER['PHP_SELF'] . '?reload');
        exit();
    }
}

return false;
}


// highlight a word after search
function doHighlight($str, $keyword) {
   $str1 = preg_replace("/\p{L}*?".preg_quote($keyword)."\p{L}*/ui", "<b>$0</b>", $str); 
   return $str1;
}


// check remote version 
function checkVersion() {
    
    if (!file_exists( ROOTPATH . '/version.txt')) { die("File version.txt does not exist"); };

    $current = file_get_contents(ROOTPATH . '/version.txt');
    $cpieces = explode("|", $current);
    if (count($cpieces) !=2) return "";

    $remote = file_get_contents('https://gitlab.com/ks_found/zwinode/-/raw/main/version.txt?ref_type=heads');
    $rpieces = explode("|", $remote);
    if (count($rpieces) !=2) return "";

    //echo $current . "  -  " .  $remote;
    if ($rpieces[0] > $cpieces[0]) {
       return $remote; 
    } 

    return ""; 
}



// create directory recursivley (if does not exists)
function createPath($path) {
    if (is_dir($path)) 
        return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    $return = createPath($prev_path);
    return ($return && is_writable($prev_path)) ? mkdir($path) : false;
}

// base URL
function baseURL(){
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
        $url = "https://";
    }else{
        $url = "http://";
    }
    // Fix
    if(dirname($_SERVER['PHP_SELF']) == "/" || dirname($_SERVER['PHP_SELF']) == "\\") {
        return $url . $_SERVER['HTTP_HOST'];
    } else {
        return $url . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
    }
}


# Remove external links and replace with plain text 
# $str - original HTML page
# $option: (0): do nothing
#          (1): all links will be plain text
#          (2): all allinks will be internal to find.php?query=     
#
function removeLink($str, $option=0){

  if ($option==0) return $str;

  $regex = '/<a (.*)<\/a>/isU';
  preg_match_all($regex,$str,$result);
  foreach($result[0] as $rs)
  {
    $regex = '/<a (.*)>(.*)<\/a>/isU';
    $text = preg_replace($regex,'$2',$rs);
    if ($option==2) $text="<a href='../find.php?query=".$text . "'>".$text. "</a>";
    $str = str_replace($rs,$text,$str);
  }
  return $str;
}


function isInteger($input){
    return(ctype_digit(strval($input)));
}



function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
 return $text;
}

 // Making a temporary dir for unpacking a zipfile into
function tempdir() {
    $tempfile=tempnam(sys_get_temp_dir(),'');
    // tempnam creates file on disk
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}

// random string with length
function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

?>

