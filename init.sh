#!/bin/bash
# Task: Initialize ZWI directory

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
echo "Initializing repository:"

ZWI_REPO=$SCRIPT_DIR/ZWI


# if user is empty, assume PHP
if [ -z "$USER" ]
then
      echo "The user $USER is PHP"
else
 if [ ! -w "$(dirname "$ZWI_REPO")" ]; then
    echo "Insufficient permissions to create $ZWI_REPO for user '$USER'"
    echo "Exit!"
    exit 1
 fi
fi



# lock the process..
PIDFILE="$SCRIPT_DIR/tmp/doinit.lock"
remove_pidfile()
{
  rm -f "$PIDFILE"
}
another_instance()
{
  echo "There is another instance running, exiting"
  exit 1
}
if [ -f "$PIDFILE" ]; then
  kill -0 "$(cat $PIDFILE)" && another_instance
fi
trap remove_pidfile EXIT
echo $$ > "$PIDFILE"
## end lock


echo "Initialize from https://encycloreader.org/ZWINODE/ZWI/"
python3 $SCRIPT_DIR/ZWINetwork/zwi_share.py -q -i https://encycloreader.org/ZWINODE/ZWI/
$SCRIPT_DIR/index.sh

#echo $SCRIPT_DIR
#git clone https://gitlab.com/ks_found/ZWINetwork.git
#python3 $SCRIPT_DIR/ZWINetwork/zwi_share.py -q -p enhub -i https://encycloreader.org/db/ZWI/ 
#python3 $SCRIPT_DIR/ZWINetwork/make_index.py -i $ZWI_REPO 
#python3 $SCRIPT_DIR/zwi_sqlite.py
#date
#source ZWINetwork/zwi_network.sh
#zwi_get -p enhub -i https://encycloreader.org/db/ZWI/
#zwi_index -i ZWI
