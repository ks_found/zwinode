<!DOCTYPE html>
<?php
require_once("../config.php");
require_once("../comstyle.php");
?>

<html >
    <head>
       <title>Add ZWI <?php echo $conf['title'];?></title>
       <meta charset="UTF-8">
       <meta name="description" content="Add ZWI file to <?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>

<style>
.navbar-brand {
  color: #85c1e9;
}
</style>


    </head>
    <body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../">ZWINode</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

       <li class="nav-item">
          <a class="nav-link" href="../articles/">Articles</a>
        </li>

         <li class="nav-item">
          <a class="nav-link active" href="./">Edit</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">



<?php
require_once("../common.php");

$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
if (!$isPasswordCorrect) {
    die("You did not login to process this action");
};



$furl="";
if (isset($_GET['fname'])) $furl = $_GET['fname'];

if (filter_var($furl, FILTER_VALIDATE_URL) === FALSE) {
    die('Not a valid URL to the article');
}

define('ROOTPATHC', __DIR__); 
$ZWIPATH=dirname(ROOTPATHC);

if ( !is_writable( $ZWIPATH )) {
    die("<h3 style=\"color: red;\">The directory \"$ZWIPATH\" mist be writable by the server! Upload is not allowed</h3></html></body>");
}

$name = basename($furl); // to get file name
$ext = pathinfo($furl, PATHINFO_EXTENSION); // to get extension

$ext=strtolower($ext);

// ***************** if URL points directly to ZWI file
$newfile="";
if (strlen($ext)>0  && strlen($ext) < 4) {
  if ($ext  !== "zwi"){
  die('Not a valid ZWI file to the article');
  }
  
 $ffurl = str_replace("%23", "#", $furl);
 // standard for all software 
 $pieces = explode("ZWI", $ffurl);
 // for encyclosearch 
 $pieces_search = explode("database", $ffurl);
 
 if (count($pieces) !=2 &&  count($pieces_search) !=2 ){
    die('Cannot correctly decode the path to ZWI file');
 }
  $furl = str_replace("#", "%23", $furl);
  // https://encycloreader.org/ZWI/en/handwiki/handwiki.org/wiki#Engineering:CVAX.zwi
  $newfile = $ZWIPATH  . "/ZWI" . $pieces[1];
  // nonstandard for encyclosearch
  if (count($pieces_search) ==2) $newfile = $ZWIPATH  . "/ZWI" . $pieces_search[1]; 
}

// ********************** if the last part of URL is 12-character 
//php?id=rLXtWaaxrOP9
if (strlen($ext)==19) {
     $pieces = explode("?id=", $furl);
     if (strlen($pieces[1]) ==12) {
	$furl = str_replace("#", "%23", $furl);
	$furl = $furl . "&action=zwi";
        $furl = file_get_contents($furl); // get response 
        $furl = str_replace("\n", "", $furl);
	$furl = trim($furl);
	$ffurl = str_replace("%23", "#", $furl);
	$pieces = explode("ZWI", $ffurl);
	if (count($pieces) !=2){
            die('URL ends with 12-character code <h4>Cannot correctly decode the path to ZWI file</h4>'. "Read=" . $pieces[1]);
         }
        $newfile = $ZWIPATH  . "/ZWI" . $pieces[1];
      };
}

// everything else
if (strlen($newfile)<3){
        $furl = str_replace("#", "%23", $furl);
        $furl = $furl . "&action=zwi";
        $furl = file_get_contents($furl); // get response 
        $furl = str_replace("\n", "", $furl);
        $furl = trim($furl);
        $ffurl = str_replace("%23", "#", $furl);
        $pieces = explode("ZWI", $ffurl);
        if (count($pieces) !=2){
            die('Cannot correctly decode the path to ZWI file for Encycloreader or Encyclosearch.');
         }
        $newfile = $ZWIPATH  . "/ZWI" . $pieces[1];
};


//print("Source URL=". $furl. "</br>");
//print("Destination=". $newfile . "</br>");

// check directory and if it does not exist make it
$mydir = dirname($newfile);
if (createPath($mydir) == false){
      die("Error in creating a directory $mydir. No permission?"); 
};



// https://encycloreader.org/ZWI/en/handwiki/handwiki.org/wiki#Engineering:CVAX.zwi
//
if ( copy($furl, $newfile) ) {
     $command ="../index.sh index > ../tmp/index.log 2>&1";
     $output = shell_exec($command);

     if (file_exists($newfile)) { 
       $getLastModDir = filemtime( $newfile ); 
       $getsize = filesize( $newfile ); 
       $command ="python3 ./zwi_sqlite_add.py $newfile $getLastModDir $getsize  >> ../tmp/index.log 2>&1";
       $output = shell_exec($command);
     }

     //echo "<pre>".$output."</pre>";
     //echo nl2br("<pre>".$output."</pre>");
     //$output = shell_exec('grep Total install.log');
     //echo "<pre>".$output."</pre>";
     echo "<p></p><h4>The ZWI file was added <font color=\"green\">OK</font></h4>";
     echo "<form action=\"index.php\" method=\"post\" > <button type='submit'>Click to finish</button> </form>";
     die();
}else{
    $errors= error_get_last();
    echo "COPY ERROR: ".$errors['type'];
    echo "<br />\n".$errors['message'];
    die("Copy failed.. Exit");
}

?>

<footer class="bg-white text-center">
   <p><?php echo footer()  ?>   </p>
</footer>


</div>



</body>
</html>

