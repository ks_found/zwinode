#!/bin/bash
# -*- coding: utf-8 -*-
# Add a ZWI to database 
# @version 1.0. Oct 13, 2023
# S.V.Chekanov (KSF)

"exec" "python3" "-Wignore" "$0" "$@"
__version__ = '1.4'
__author__  = 'Sergei Chekanov  (KSF)'
__doc__     = 'Add a ZWI file to SQL database'

import re,sys,os
import gzip
import sqlite3
from time import time
import tempfile
import zipfile
import json
from datetime import timezone
import datetime
import hashlib
import base64
from pathlib import Path

# Getting the current date in UTC
dt = datetime.datetime.now(timezone.utc)
utc_time = dt.replace(tzinfo=timezone.utc)
utc_timestamp = int(utc_time.timestamp())

# Create a short, fairly unique, urlsafe hash for the input string.
# https://roytanck.com/2021/10/17/generating-short-hashes-in-php/
def generate_id( input_str, length = 12 ):
   hash_base64 =  (base64.b64encode(hashlib.sha256(input_str.encode('utf-8')).digest()))
   hash_base64=hash_base64.decode('ascii')
   # // Replace non-urlsafe chars to make the string urlsafe.
   hash_urlsafe  = hash_base64.replace('+', '0');
   hash_urlsafe  = hash_urlsafe.replace('/', 'a');
   hash_urlsafe  = hash_urlsafe.replace('-', 'b');
   hash_urlsafe  = hash_urlsafe.replace('_', 'c');
   # do not start has from -. Replace with 0
   hash_urlsafe=hash_urlsafe.strip()
   return hash_urlsafe[0:length]

# Add ZWI to database 
# Input: full path to ZWI   
#  
def addZWI2SQL(path2zwi, timestamp, filesize):

    script_dir=os.path.dirname(os.path.realpath(__file__))
    path = Path(script_dir)
    basedirZWI=str(path.parent.absolute())+"/ZWI/en"
    #print("basedirZWI = ",basedirZWI)

    # output database
    DB=basedirZWI+"/index.sqlite"

    print("SQLite database=",DB);
    # table
    table="dbase_zwi"

    print("Add=",path2zwi);

    conn = sqlite3.connect(DB) # or use :memory: to put it in RAM
    cursor = conn.cursor()

    if (os.path.exists(path2zwi) == False):
                 print("Skip file -> Cannot find =",path2zwi);
                 return 


    zwi_file=None
    try:
        zwi_file = zipfile.ZipFile(path2zwi, "r")
    except zipfile.error as e:
        print("Bad zip:", path2zwi)
        return 
    if (zwi_file==None): return 


    metadata=""
    try:
      metadata = zwi_file.read("metadata.json")
    except zipfile.BadZipFile as eb:
        print("Bad ZIP when reading metadata.json. Skip:",path2zwi)
        return 
    except KeyError:
        print("Cannot read metadata.json. Skip:",path2zwi)
        return 
    zwi_file.close()
    if (len(metadata)<5): return 

    jdata = json.loads( metadata )
    data=path2zwi.replace(".zwi","")
    xpath=data.split("/")

    x1=xpath[len(xpath)-1] # name of file 
    x2=xpath[len(xpath)-2] # domain name 
    x3=xpath[len(xpath)-3] # publisher 
    #x4=xpath[len(xpath)-4]
    path=x3+"/"+x2+"/"+x1  # only encyclopedia/md5[0]/md5[0:1]/name 
    # print("Path for SQLITE=",path)
    if path.find("trash/")>-1:
                print("Trush was found",path)
                return
    title=jdata["Title"];
    title=title.replace("_"," ")
    stop=title.split(":",1);
    if (len(stop)>1):
            title=stop[1]
    title=title.title();

    publisher="handwiki"
    if "Publisher" in  jdata:
           publisher=jdata["Publisher"];
           publisher=publisher.lower();

    description=""
    if "Description" in  jdata:
           description=jdata["Description"];

    license=0 # free license, open in view.php. Non-free license - redirect 
    if (publisher == "sep"): license=1; # non-free
    #if (publisher == "isbe"): license=1; # non-free
    if (publisher == "whe"): license=1; # non-free
    if (publisher == "greatplains"): license=1; # non-free
    # if (publisher == "jewishenc"): license=1; # non-free

    lang="en";
    if "Lang" in  jdata:
             lang=jdata["Lang"];


    rating=[0,0]
    if "Rating" in  jdata:
       rating=jdata["Rating"];

    srating="0,0"
    if type(rating) == list:
          srating="";
          for i in range(len(rating)):
              if (i==0): srating=str(rating[i])
              else: srating=srating+","+str(rating[i])
    else:
        print("TypeError: Wrong list of indices in Rating:",fullpath)
        srating="0,0";


    #topics="";
    #if "Topics" in  jdata:
    #   topics=jdata["Topics"];

    #sourceurl="";
    #if "SourceURL" in  jdata:
    #   sourceurl=jdata["SourceURL"];
    #   sourceurl=sourceurl.replace("https://en.citizendium.org", "https://citizendium.org")
    #categories=jdata["Categories"]; 
    #topic=','.join(topics) 
    #categories=','.join(categories)
    #categories=""; 
    #comments="";

    # print("Path=",path);
    # unique hash 

    # remove first slash
    pathH=path;
    if (pathH[0] == "/"): pathH=path[1:]
    shash=generate_id(pathH);
    print(pathH,shash)

    if (str(timestamp).isdigit() == False):
                         print("timestamp is not digit! Skip");
                         return 
    if (str(filesize).isdigit() == False):
                         print("filesize is not digit! Skip");
                         return 

    try:
         cursor.execute("INSERT INTO '"+table+"' (path,title,publisher,hash,filesize,timestamp,license,rating,description) VALUES (?,?,?,?,?,?,?,?,?)", (path,title,publisher,shash,filesize,timestamp,license,srating,description))
    except sqlite3.Error as my_error:
           print("error: ",my_error, " ", path)


    conn.commit()
    conn.close()
    print("Added=",path2zwi)



if __name__ == "__main__":
  print ('Number of arguments:', len(sys.argv), 'arguments.') 
  if (len(sys.argv) != 4):
       print("Arguments: filepath timestamp filesize") 
       sys.exit()
  path2zwi=sys.argv[1]
  timestamp=sys.argv[2]
  filesize=sys.argv[3]
  print("Path2ZWI=",path2zwi)
  print("Time=",timestamp)
  print("Size=",filesize)
  addZWI2SQL(path2zwi, timestamp, filesize)

