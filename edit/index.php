<!DOCTYPE html>
<?php
require_once("../config.php");
require_once("../comstyle.php");
?>

<html >
    <head>
       <title>Edit <?php echo $conf['title'];?></title>
       <meta charset="UTF-8">
       <meta name="description" content="Edit <?php echo $conf['tagline'];?>">
       <meta name="keywords" content="Edit ZWINode articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="KSF">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>

<style>
.navbar-brand {
  color: #85c1e9;
}

.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #85c1e9;
}

h1 {
font-size: 28px;
font-weight: 600;
line-height: 1.4;

}

h2 {
font-size: 24px;
font-weight: 600;
line-height: 1.4;
margin-top: 20px;
color: #6995b3;
}

pre {
  font-size: 1em;
  border: 2px solid grey;
  width: 450px;
  border-left: 10px solid #6495ED;
  border-radius: 5px;
  padding: 14px;
}


</style>


    </head>
    <body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../"><?php echo $conf['title'];?></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

       <li class="nav-item">
          <a class="nav-link" href="../articles/">Articles</a>
        </li>

       <li class="nav-item">
          <a class="nav-link" href="../editor/">+Add</a>
        </li>

         <li class="nav-item">
          <a class="nav-link active" href="./">Edit</a>
        </li>

	<li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search"> 
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">


<?php

require_once("../common.php");

$login=-1;
if (isset($_GET['login']))  $login =$_GET['login'];
if ($login == "0") {

$txt = <<<EOD
<center>

  <form action="index.php?login=1" method="post">

  <label for="pass">Password (5 characters minimum):</label>
  <input type="password" id="pass" name="pass" minlength="5" required />

<p>
</p>

<input type="submit" value="Sign in" />

  </form>

</center>
EOD;
echo $txt;

// check version
$xv=checkVersion();
if (strlen($xv)>1){
     echo "<p style=\"color: red\">New version of ZWINode is available</p>" . $xv; 	
}	

echo "</div> </body>\n</html>";
dir();

// check login form
} else if ($login == "1") {

 //echo "Checking login";
 $pass="";
 if (isset($_POST['pass']))  $pass =$_POST['pass'];
 if ($pass == $conf['password']) {
         $hash = password_hash($pass, PASSWORD_DEFAULT);  
          // remove files and directories which are 1 day old
          $command ="nohup ./clear_tmp.sh > ../tmp/clear_tmp.log 2>&1 &";
          $output = shell_exec($command);
          // set cookies 
          setcookie("zwinode", $hash, time()+10800, "/");  /* expire in 3 days */ 
	  header('Location:index.php'); 

 } else {
        print("<span class=\"error\">Login was not successful!</span><br>"); 	 
	 
 };

// logout
}  else if ($login == "3") {
      setcookie("zwinode", "", time()-3600, "/");
      header('Location:index.php');
};	


$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
//echo "Is correct=" . $isPasswordCorrect;

// if password correct
if ($isPasswordCorrect ) { 
$URL1="location.href='index.php?login=3';";
$extrabutton1 = <<<EOD
<div style="margin-top:2px; float:right;  top: 15px; font-size:14px;">
<button class="hwbutton" onclick="$URL1">Logout</button>
</div>
EOD;
echo $extrabutton1;
} else {

//  check password OK
$URL2="location.href='index.php?login=0';";
$extrabutton2 = <<<EOD
<div style="margin-top:2px; float:right;  top: 15px; font-size:14px;">
<button class="hwbutton" onclick="$URL2">Login</button>
</div>
EOD;
echo $extrabutton2;
}	

// common login button
$URL="location.href='index.php?login=0';";
$extrabutton3 = <<<EOD
<div style="margin-top:2px; font-size:14px;">
<button class="hwbutton" onclick="$URL">Login</button>
</div>
EOD;



$init ="";
if (isset($_GET['e']))  $init =$_GET['e'];
$init=trim($init);
if ($init == "ini") {
     if ( !is_writable( "../ZWI/" )) {
       die("<h3 style=\"color: red;\">The directory \"ZWI\" mist be writable by the server! Indexing is not allowed</h3></html></body>");
     }

      echo "Reindex repository: <br>";
      $command ="../index.sh > index.log 2>&1";
      $output = shell_exec($command);
      //echo "<pre>".$output."</pre>";
      //echo nl2br("<pre>".$output."</pre>");
      //$output = shell_exec('grep Total install.log');
      //echo "<pre>".$output."</pre>";
      echo "<h3>Indexing was started <font color=\"green\">OK</font></h3>";
      echo "<form action=\"./index.php\" method=\"post\" > <button type='submit'>Click to finish</button> </form>";
      die();
}
?>



<?php
if(is_dir('../ZWI') == false) {
	die("<h2>Your ZWINode has not been installed yet! </h2> </body> </html> ");
} 
?>

<p>
</p>
<h2>Sharing your articles with others</h2>


The ZWI files of this node are located in this <a href="../ZWI/">ZWI sharing point</a>.
Use this link for checking your locally stored ZWI files and for sharing with other nodes.

<?php

if (!$isPasswordCorrect) {
$footnote=footer();
$txt = <<<EOD

<p>
To modify this site  by adding additional articles from similar nodes,
use the top-right button to login to the maintanace mode.
</p>

<p>
</p>

</dev>

<footer class="bg-white text-center">
<p>$footnote</p>
</footer>

</body>
</html>
EOD;
echo $txt;
die();
}; 
?>

<p>
</p>
<h2>Host an article</h2>
If you would like  to add a ZWI article to your site from other ZWInode(s), <a href="https://encycloreader.org">encycloreader.org</a> or <a href="https://encyclosearch.org">encyclosearch.org</a> etc.,  
copy and paste the URL
of the article viewed in external ZWINodes. The article will be added to your collection:

<p>
</p>

<?php 

if ($isPasswordCorrect) {
$txt = <<<EOD
<form action="addzwi.php">
  <label for="fname"><i>URL to the article from an external ZWINode:</i></label><br>
  <input type="text" id="fname" name="fname"  placeholder="URL to ending with .zwi, 12-character code, etc.." ><br>
  <input type="submit" value="Add this article to your node">
</form>
EOD;
echo $txt;
} else { 
echo "Please login to enable this option. ";
echo $extrabutton3;
}

?>


<p>
</p>

<h2>Upload an article</h2>
If you have an article in the <a href="https://docs.encyclosphere.org/#/zwi-format">ZWI format</a> 
as defined by Encyclosphere,
add this article to your collection:
<p>
</p>


<?php
if ($isPasswordCorrect) {
$txt = <<<EOD
<form action="uploadzwi.php" method="post" enctype="multipart/form-data">
  <input type="file" name="zwi" id="zwi">
  <p></p>
   <input type="submit" value="Upload ZWI file" name="submit">
</form>
EOD;
echo $txt;
} else { 
echo "Please login to enable this option. ";
echo $extrabutton3;
}
?>


<p>
</p>


<h2>Create replicas of other publishers</h2>
If you  would like to create complete replicas of many articles of  other publisher(s),
use the command line tools.
<p>
</p>

<pre>
source ZWINetwork/zwi_network.sh
zwi_get -p [PUBLISHER] -i [URL]
./index.sh
</pre>

where [PUBLISHER] and [URL] are publisher name and its [URL].
<a href="https://gitlab.com/ks_found/ZWINetwork">ZWINetwork</a> for more details. 
Make sure you have sufficient permission to write to the directory "ZWI".


<h2>Re-indexing your collection</h2>
If you added some files to the directory "ZWI", or you did upgrade, then you should 
re-index your ZWI collection in the  
<a href="../ZWI/">ZWI sharing point</a>.  Click  the button below:

<div style="margin-top:10px; margin-bottom:10px">
<form action="index.php?e=ini" method="post" > <button type='submit'>Re-index repository</button> </form>
</div>
If your ZWI collection is large, this can take some time. The indexing can take a few minutes for 1000 articles.


<p>
</p>

</div>

<footer class="bg-white text-center">
    <p><?php echo footer()  ?>   </p>
</footer>

</body>
</html>
