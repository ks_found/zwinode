<!DOCTYPE html>

<?php
require_once("../config.php");
require_once("../comstyle.php");
?>


<html >
    <head>

       <title>About <?php echo $conf['title'];?></title>
       <meta charset="UTF-8">
       <meta name="description" content="<?php echo $conf['tagline'];?>">

       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>

<style>
.navbar-brand {
  color: #85c1e9;
}
</style>


    </head>
    <body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../">ZWINode</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

       <li class="nav-item">
          <a class="nav-link" href="../articles/">Articles</a>
        </li>

        <li class="nav-item">
          <a class="nav-link active" href="./">Edit</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">



<?php
require_once("../common.php");

$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
if (!$isPasswordCorrect) {
    die("You did not login to process this action");
};


define('ROOTPATHC', __DIR__); 
$ZWIPATH=dirname(ROOTPATHC);

if ( !is_writable( $ZWIPATH )) {
    die("<h3 style=\"color: red;\">The directory \"$ZWIPATH\" mist be writable by the server! Upload is not allowed</h3></html></body>");
}


$bname="";
if (isset($_FILES["zwi"]["name"]) )
        $bname=basename($_FILES["zwi"]["name"]);
else
        die("This file is not set or too large.");

$target_dir = $ZWIPATH . "/tmp/";
if (createPath($target_dir) == false){
      die("Error in creating a directory $target_dir. No permission?");
};
$target_file = $target_dir . str_replace("#", "-", $bname);
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$shortname = str_replace(".zwi", "", $bname);


$uploadOk = 1;
$nline="<br>";


if (strlen( $bname )>3){
  // Check file size
  if ($_FILES["zwi"]["size"] > 30000000) {
    echo "Error: The file is too large (more than 30MB) $nline";
    $uploadOk = 0;
  }

    // Check if file already exists
  if (file_exists($target_file)) {
    echo "Error: ".$bname." file already exists $nline";
    $uploadOk = 0;
  }


// Allow certain file formats
 if($imageFileType != "zwi") {
    echo "Error: only ZWI files are allowed. $nline";
    $uploadOk = 0;
 }
} // end basic checks 


// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    die("Sorry, your file was not uploaded due to previous errors");
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["zwi"]["tmp_name"], $target_file)) {
         print("Uploading the file..");

        $fsize=filesize($target_file);

        $fzip = new ZipArchive;
        $decoded_json="";
        $res = $fzip->open($target_file);
        //die( $target_file );

        if ($res === TRUE) {
              $metadata = $fzip->getFromName("metadata.json");
              $decoded_json=json_decode($metadata,true);
              //print("Read ZIP");
        } else{
              die("Error: Cannot read metadata of this ZWI file");
        }
        $fzip->close();


       $publisher=$decoded_json['Publisher'];
       $publisher=strtolower($publisher);
       $title=$decoded_json['Title'];
       $license_str=$decoded_json['License'];
       $xtime=$decoded_json['TimeCreated'];
       $lang=$decoded_json['Lang'];
       $sourceURL="";
       if( isset( $decoded_json['SourceURL'] ) ) {
          $sourceURL= $decoded_json['SourceURL'];
        };

       if (strlen($title)<2) {unlink($target_file);  die("No title is defined $nline");  }
       if (strlen($publisher)<2) {unlink($target_file);  die("No publisher is defined $nline"); }
       if (strlen($license_str)<2) {unlink($target_file); die("No license is defined $nline"); }
       if (strlen($xtime)<2) {unlink($target_file); die("No TimeCreated is defined $nline"); }
       if (strlen($lang)<2) {unlink($target_file); die("No Lang is defined $nline"); }
       if(is_numeric($xtime) == false) {unlink($target_file); die("TimeCreated is not integer:". $xtime . $nline); }

      //echo "- From domain: " . $domain['path'] . $nline;
       echo "- Input file: " . $shortname . ".zwi $nline";
       echo "- File size (B): " . $fsize . $nline;
       echo "- Article title: " . $title . $nline;
       echo "- Publisher: " . $publisher .  $nline;
       echo "- License: " . $license_str .  $nline;
       echo "- Lang: " . $lang .  $nline;

       $domain="default";
       if ($sourceURL != null) { 
                if (strlen($sourceURL)>4) {
                    $parse = parse_url($sourceURL);
                    $domain= $parse['host']; //   handwiki.org
                    $path=$parse['path']; //   wiki/Company:Fleetmatics
                    # extract path "wiki"
                    $parts=explode("/", $path);
                    $dd="";
                    $nn=0;
                    foreach($parts as $item) {
                     if (strlen($item)>0 && $nn<count($parts)-1) {
                         if ($nn==1) $dd = $item;
                         else $dd=$dd . "#".  $item;
                         };
                         $nn = $nn+1;
                         }
                     if (strlen($dd)>0) $subdir=$dd;
                };
       };


       $shortname=str_replace(" ", "_", $shortname);
       $xdir = $publisher . "/" . $domain;
       $xdir_zwi = $xdir  . "/" . $shortname . ".zwi";

       $fdir = $ZWIPATH ."/ZWI/en/" . $xdir;
       $newfile = $ZWIPATH ."/ZWI/en/" . $xdir_zwi;
       //echo "- Saved to: " . $newfile . $nline;
       echo "- Saved to:"  . "ZWI/en/" . $xdir_zwi . $nline;

// check directory and if it does not exist make it
if (createPath($fdir) == false){
      die("Error in creating a directory $fdir. No permission?"); 
};


       $err = rename($target_file, $newfile);
       if ($err == false){
               unlink($target_file);
               die("Error: Problems with moving to the correct folder." . $nline);
       } else {

        $command ="../index.sh index > ../tmp/index.log 2>&1";
        $output = shell_exec($command);

     if (file_exists($newfile)) { 
       $getLastModDir = filemtime( $newfile ); 
       $getsize = filesize( $newfile ); 
       $command ="python3 ./zwi_sqlite_add.py $newfile $getLastModDir $getsize  >> ../tmp/index.log 2>&1";
       $output = shell_exec($command);
     }

     //echo "<pre>".$output."</pre>";
     //echo nl2br("<pre>".$output."</pre>");
     //$output = shell_exec('grep Total install.log');
     //echo "<pre>".$output."</pre>";
     echo "<p></p><h4>The ZWI file was uploaded <font color=\"green\">OK</font></h4>";
     echo "<form action=\"index.php\" method=\"post\" > <button type='submit'>Click to finish</button> </form>";
     die();
     }

 }}


?>

</div>

<div class="bg-white text-center">
</br>
<p><?php echo footer();  ?>   </p>
</div>

</body>
</html>

