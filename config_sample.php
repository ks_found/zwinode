<?php
/*
 * Main Configuration File - Local Settings
 * Date: Sat, 01 Nov 2023 09:45:30 -0400
*/

# main title of the site and its tagline
$conf['title'] = 'ZWINode';
$conf['tagline'] = 'Decentralized Publishing Platform';

# password to perform maintenance actions, such us import ZWI files, index repository etc.
# You must change this password otherwise you cannot install ZWINode
$conf['password'] = 'zwinode';

# by default, search is performed in titles only.
# you can enable search in description of articles too
# if you replace 0 with 1
$conf['search_in_descriptions'] = 0;

# by default, the numbers of views are counted for each article, 
# and is shown at the end of articles.
# If you do not want this feature to speed up the website, set this value to 0;
$conf['view_count'] = 1;

# by default, creation of articles by anonymous users are disabled. 
# If you want to allow all people to create articles, set the value to 1
$conf['create_article'] = 0;



?>
