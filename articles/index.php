<!DOCTYPE html>

<?php
require_once("../config.php");
require_once("../comstyle.php");
?>

<html >
    <head>
       <meta charset="UTF-8">
       <title>Articles of <?php echo $conf['title'];?></title>
       <meta name="description" content="List of articles from <?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" type="text/css" href="../css/datatables.css"/>
       <style type="text/css" title="currentStyle">
                @import "../css/demo.css";
                </style>
                <script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
                <script type="text/javascript" src="../js/datatables.min.js"></script>
                <!-- Start here -->
                <?php
                 include "serverlist.php";   // this class handles is both server and client side data

                 //now generate the datatables Jquery with this SQL here:
                 //for added security move this array into to the serverside client
                 $db_array=array(
                                "sql"=>'SELECT rowid,title,publisher,filesize,timestamp,hash FROM dbase_zwi', /* Spell out columns names no SELECT * Table */
                                "table"=>'dbase_zwi', /* DB table to use assigned by constructor*/
                                "idxcol"=>'rowid' /* Indexed column (used for fast and accurate table cardinality) */
                                        );
                $javascript = ServerDataPDO::build_jquery_datatable($db_array);
                echo $javascript;
                ?>

<style>
.navbar-brand {
  color: #85c1e9;
}

body {
  margin: 0;
  font-size: 16px;
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
}


.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #85c1e9;
}

h1 {
font-size: 32px;
font-weight: 600;
line-height: 1.4;

}

h2 {
font-size: 26px;
font-weight: 600;
line-height: 1.4;
margin-top: 10px;
color: #6495ED; 
}

pre {
  font-size: 1em;
  border: 2px solid grey;
  width: 450px;
  border-left: 10px solid #6495ED;
  border-radius: 5px;
  padding: 14px;
}


</style>


    </head>
    <body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../"><?php echo $conf['title'];?></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

        <li class="nav-item">
          <a class="nav-link active" href="./">Articles</a>
        </li>

       <li class="nav-item">
          <a class="nav-link" href="../editor/">+Add</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="../edit/">Edit</a>
        </li>

	<li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search"> 
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">

<div style="margin-top:30px;margin-bottom:30px;"> 
This page lists all articles of your website. 
You can search for article's titles, view them and download
on your computer. The symbol "M" in the column "ZWI" is used to indicate that this ZWI file contains only metadata, but not the content. To remove articles from this list,  <a href="../edit/index.php?login=0">login</a> and come back to this panel. You will see the red buttons which can be used to remove articles. The numbers of views of the ZWI files <a href="nrviews.php">shown here</a>. 
</div>


<?php
if(is_dir('../ZWI') == false) {
     die("<h2>No articles.  This node has not been installed yet! </h2> </body> </html> ");
}

require_once("../config.php");
require_once("../common.php");


// remove action
$action="";
if (isset($_GET['a']))  $action =$_GET['a'];
if ($action =="d"){

$id="";
if (isset($_GET['id']))  $id =$_GET['id'];
if (strlen($id)==12){

// check again 
$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
if (!$isPasswordCorrect) {
    die("You did not login to process this action");
};


$db = null;
try {
   $db = new PDO($databasefile);
} catch( PDOException $e ) {
        echo "Error to open database";
        die( $e->getMessage() );

}

$sql = "SELECT path,title,publisher FROM " . $sTable . " WHERE hash='$id'" . " LIMIT 1;";
if ($sth = $db->prepare($sql)) {
           $sth->execute();
}

$rowarray = $sth->fetchall(PDO::FETCH_ASSOC);
$rowno = 0;

$db = null;


$title="";
$publisher="";
$zfile="";

foreach($rowarray as $row) {
if (isset($row['path']))
        $zfile=$row['path'];

if (isset($row['title']))
        $title=$row['title'];

if (isset($row['publisher']))
        $publisher=$row['publisher'];

}

$title=str_replace("_", " ", $title);

echo "<h4>Do you want remove the entry \"$title\" from the \"$publisher\" publisher? </h4>";
echo "<p></p><form action=\"index.php?id=$id&a=dy\" method=\"post\" > <button type='submit'>Yes, remove</button> </form>";
echo "<p></p><form action=\"index.php\" method=\"post\" > <button type='submit'>No, keep</button> </form>"; 

die();
}

} // end action 


// final removal action
$action="";
if (isset($_GET['a']))  $action =$_GET['a'];
if ($action =="dy"){

$id="";
if (isset($_GET['id']))  $id =$_GET['id'];
if (strlen($id)==12){

// check again 
$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
if (!$isPasswordCorrect) {
    die("You did not login to process this action");
};


$db = null;
try {
   $db = new PDO($databasefile);
} catch( PDOException $e ) {
        echo "Error to open database";
        die( $e->getMessage() );

}

$sql = "DELETE FROM ".  $sTable . " WHERE hash= :hash;";
//die($sql);

$sth = $db->prepare($sql);
if ($sth) {
        $sth->bindValue(':hash', $id);
        $result  = $sth->execute();
        if (!$result) {
          print_r($result->errorInfo()); // if any error is there it will be posted
        }
} else {
   echo '<br>Prepare failed';
}

$db = null;

//reindex all
$command ="nohup ../index.sh index > ../tmp/index.log 2>&1 &";
$output = shell_exec($command);

header('Location:index.php');
//header('Location: index.php');
//die();

}
}



	$cols="Nr,Title,Publisher,kB,Timestamp,ZWI";  //Column names for datatable headings (typically same as sql)
        $html =ServerDataPDO::build_html_datatable($cols,$table_id="datatable1");
        echo  $html;
?>

<p>
</p>


</div>

<footer class="bg-white text-center">
    <p><?php echo footer()  ?>   </p>
</footer>

</body>
</html>
