<!DOCTYPE html>

<?php
require_once("../config.php");
require_once("../comstyle.php");
?>


<html >
    <head>
       <title>Views of  <?php echo $conf['title'];?></title>
       <meta charset="UTF-8">
       <meta name="description" content="List of articles from <?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.navbar-brand {
  color: #85c1e9;
}

body {
  margin: 0;
  font-size: 16px;
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
}


.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #85c1e9;
}

h1 {
font-size: 32px;
font-weight: 600;
line-height: 1.4;

}

h2 {
font-size: 26px;
font-weight: 600;
line-height: 1.4;
margin-top: 10px;
color: #6495ED; 
}

pre {
  font-size: 1em;
  border: 2px solid grey;
  width: 450px;
  border-left: 10px solid #6495ED;
  border-radius: 5px;
  padding: 14px;
}


</style>


    </head>
    <body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../"><?php echo $conf['title'];?></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

        <li class="nav-item">
          <a class="nav-link active" href="./">Articles</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="../edit/">Edit</a>
        </li>

	<li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search"> 
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">

<div style="margin-top:30px;margin-bottom:30px;">
This page shows 500 articles with the largest numbers of views.
</div>


<table class="table">
    <tr>
        <th>Title</th>
        <th>Publisher</th>
        <th>Views</th>
    </tr>

<?php

// logging page hits
$dbname = "../viewlog.sqlite3";

if(file_exists($dbname))
{
  $logdb = new PDO("sqlite:".$dbname);
  $statement = $logdb->query("SELECT page,counter,title,publisher FROM hits ORDER BY counter DESC LIMIT 500;");
  $records = $statement->fetchAll();
  foreach($records as $row) {
          $page=$row['page'];
          $title="<a href=\"../view.php?id=$page\">" .  $row['title'] ."</a>";
          echo("<tr> <td>". $title." </td> <td>". $row['publisher'] ." </td><td>" . $row['counter'] ."</td> </tr>");
  }
}


?>

    </table>
    </div>



<p>
</p>

<footer class="bg-dark text-center text-white">
   <p><?php echo footer()  ?>   </p>
</footer>


</div>




</body>
</html>
