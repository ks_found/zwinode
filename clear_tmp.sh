#!/bin/bash
# Task: clear the installation 

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TMP_REPO=$SCRIPT_DIR/tmp/
echo "Current TMP directory: $TMP_REPO" 
find $TMP_REPO -type d -ctime +1 -exec rm -rf {} \;
echo "Removed all TMP directories and files which are 1 day old"
