#!/bin/bash
# -*- coding: utf-8 -*-
# Main program to build SQLite3 database 
# Run: python3 zwi_sqlite.py  
# @version 1.0. Nov 25, 2023
# S.V.Chekanov (KSF)


"exec" "python3" "-Wignore" "$0" "$@"

__version__ = '1.4'
__author__  = 'Sergei Chekanov  (chekanov@jwork.org)'
__doc__     = 'Create a ZWI SQL database from scratch'

import re,sys,os
from pathlib import Path
import sqlite3
from time import time
import tempfile
import zipfile
import json
from datetime import timezone
import datetime
import hashlib
import base64

# Getting the current date in UTC 
dt = datetime.datetime.now(timezone.utc)
utc_time = dt.replace(tzinfo=timezone.utc)
utc_timestamp = int(utc_time.timestamp())

# ZWI directory
basedirZWI= os.path.dirname(os.path.realpath(__file__))+ "/ZWI/en"
# output database
DB=basedirZWI+"/index.sqlite"
print("Open database:",DB)

# table
table="dbase_zwi"

# Max number of ZWI files. Add -1 for all files
NMaxNumber=-1

# Create a short, fairly unique, urlsafe hash for the input string.
# https://roytanck.com/2021/10/17/generating-short-hashes-in-php/
def generate_id( input_str, length = 12 ):
   hash_base64 =  (base64.b64encode(hashlib.sha256(input_str.encode('utf-8')).digest()))
   hash_base64=hash_base64.decode('ascii')
   # // Replace non-urlsafe chars to make the string urlsafe.
   hash_urlsafe  = hash_base64.replace('+', '0');
   hash_urlsafe  = hash_urlsafe.replace('/', 'a');
   hash_urlsafe  = hash_urlsafe.replace('-', 'b');
   hash_urlsafe  = hash_urlsafe.replace('_', 'c');
   # do not start has from -. Replace with 0
   hash_urlsafe=hash_urlsafe.strip()
   return hash_urlsafe[0:length]


script_dir=os.path.dirname(os.path.realpath(__file__))
print("From = ",script_dir)
os.system("rm -f " + DB);

conn = sqlite3.connect(DB) # or use :memory: to put it in RAM
cursor = conn.cursor()

cursor.execute("PRAGMA foreign_keys=OFF;")
conn.commit()
cursor.execute("""PRAGMA encoding = "UTF-8";""")
conn.commit()

table="dbase_zwi"
print("Created table=",table)
sql="CREATE TABLE '"+table+"' ('path' VARCHAR(255) UNIQUE, 'title' VARCHAR(255),'publisher' VARCHAR(255),'hash' VARCHAR(12) NOT NULL,'filesize' INTEGER, 'timestamp' INTEGER, 'license' INTEGER, 'rating' VARCHAR(64), 'description' VARCHAR(255), PRIMARY KEY('hash') );"
cursor.execute(sql)
conn.commit()

ZWIexist={}
ZWIsorted={}

for path in Path(basedirZWI).rglob('*.zwi'):
    zwifilename=str(path)
    if (os.path.exists(zwifilename)):
       #print("Read ZWI: ",zwifilename)
       xtime=int(os.path.getmtime( zwifilename ))
       xsize=int(os.path.getsize(  zwifilename ))
       name=zwifilename.replace(basedirZWI,"")
       xfi=(name.replace(".zwi","")).strip()  
       ZWIexist[xfi] = ( xtime, xsize)  # timestamp and filesize 
       ZWIsorted[xfi] = xsize

print("Read=",len(ZWIexist))
# sort in increasing order of timing
sort_orders = sorted(ZWIsorted.items(), key=lambda x: x[1], reverse=False)

pubs={}
fid=0;
totsize=0;

for key in sort_orders: 
    key=key[0];
    fullpath=basedirZWI + "/"+  key + ".zwi"

    if (os.path.exists(fullpath) == False):
                 print("Skip file -> Cannot find =",fullpath);
                 continue


    zwi_file=None
    try:
            zwi_file = zipfile.ZipFile(fullpath, "r")
    except zipfile.error as e:
            print("Bad zip:", fullpath )
            continue
    if (zwi_file==None): continue
   
    metadata=""
    try:
      metadata = zwi_file.read("metadata.json")
    except zipfile.BadZipFile as eb:
        print("Bad ZIP when reading metadata.json. Skip and remove:",fullpath)
        os.remove( fullpath  )
        continue
    except KeyError:
        print("Cannot read metadata.json. Skip and remove:",fullpath)
        os.remove( fullpath  )  
        continue
    zwi_file.close()
    if (len(metadata)<5): continue

    jdata = json.loads( metadata )
    
    if (fid%10 == 0): print("Process=",fid)
    data=ZWIexist[key]
    path=key; # xpath[1] # only encyclopedia/md5[0]/md5[0:1]/name 
    fid=fid+1
    ID=fid
    title=jdata["Title"];
    title=title.replace("_"," ")
    stop=title.split(":");
    if (len(stop)>1):
            title=stop[1]
    title=title.title();


    publisher="enhub"
    if "Publisher" in  jdata: 
           publisher=jdata["Publisher"]; 
           publisher=publisher.lower(); 


    # count publishers
    if publisher in pubs:
        pubs[publisher] = pubs[publisher] +1;
    else: 
        pubs[publisher] = 1

    description=""
    if "Description" in  jdata:
           description=jdata["Description"];

    license=0 # free licens can be opened in view.php 

    pathH=path;
    if ( pathH[0] == "/"): pathH=path[1:]
    shash=generate_id(pathH);
    #for debugging 
    print(pathH,shash)

    rating=[0,0]
    if "Rating" in  jdata:
       rating=jdata["Rating"]; 

    srating="0,0"
    if type(rating) == list:
          srating="";
          for i in range(len(rating)):
              if (i==0): srating=str(rating[i])
              else: srating=srating+","+str(rating[i])
    else:
        print("TypeError: Wrong list of indices in Rating:",fullpath)
        srating="0,0";

    timestamp=data[0];
    if (str(timestamp).isdigit() == False): 
                         print("timestamp is not digit! Skip");
                         continue
    filesize=data[1];
    if (str(filesize).isdigit() == False): 
                         print("filesize is not digit! Skip");
                         continue
    totsize=totsize+int(filesize) 
    comments="";
    try:
      cursor.execute("INSERT INTO '"+table+"' (path,title,publisher,hash,filesize,timestamp,license,rating,description) VALUES (?,?,?,?,?,?,?,?,?)", (path,title,publisher,shash,filesize,timestamp,license,srating,description))
    except sqlite3.Error as my_error:
           print("error: ",my_error, " ", path)


conn.commit()
conn.close()

print("Written=",fid," files")
#print(json.dumps(pubs, indent=4, sort_keys=True))
for key, value in pubs.items():
    print("##", key, ":", value," articles")
print( "##",int(totsize*0.001)," kB size")
print("Created=",DB)
