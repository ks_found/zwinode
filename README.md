# Table of Contents


The encyclosphere network consists of multiple "nodes" which can be installed by anyone. 
The goal of each node is to:

- Maintain a local repository of ZWI files 
- Index this repository to prepare for download by external nodes
- Index the local repository for internal search
- View locally stored ZWI files 
- Add new ZWI files to the local repository
- Create complete replicas of other ZWI nodes if needed

Examples of such nodes are [EncycloReader](https://encycloreader.org) and [EncycloSearch](https://encyclosearch.org), which are specialized network nodes run by the KSF in order to: (1) initialize the network; (2) provide some common services; (3) collect initial ZWI files from existing encyclopedias; 
Unlike these resources, ZWINode can be run by anyone. Generally, EncycloReader and EncycloSearch are not needed for ZWINode sites.

[[_TOC_]]

Here is the description of how to setup a single node for showing and searching download encyclopedia articles from the encyclosphere network. Articles created by the [EncycloReader](https://encycloreader.org) or similar nodes can be sent to this network using the URL for file exchange.

The screenshot of the installed website is shown below. You can play with [the demo here](https://enhub.org/zwinode). The password for this demo is "12345". 

![ZWINode screenshot](images/zwinode_screen.png)

## How to Install

You need several things: A Linux OS (Ubuntu, for example) with an Apache web server serving the directory "/var/www/html/".  
This setup requires Linux since it uses several bash scripts. You should enable PHP with the PDO, ZIP and Sqlite3 extensions. 
The Linux server should have Python3 installed. For example, for Ubuntu servers, run these commands: 

```
sudo apt install python3
sudo apt install php-common php-sqlite3 php-apcu php-zip
sudo service apache2 restart
```

If you do not know what is installed on your computer, just try to install a ZWINode using the steps shown below. The installation will report any problems and suggest the solutions.
First, go to the directory served by Apache (/var/www/html/, for example), and run the command:

```
git clone https://gitlab.com/ks_found/zwinode.git
cd zwinode
git clone https://gitlab.com/ks_found/ZWINetwork.git
```
Then copy the example configuration file to "config.php": 

```
cp config_sample.php config.php
```

and edit the "config.php" file. This file can be used to define the name of your web site and the tagline. You should also specify the password for administrative tasks. By default, search is performed in title only. You can change this behaviour. To make your website faster, disable page views.

Then make sure the server has the sufficient permissions to write in this directory. Go up to one level, and set the permissions:

```
cd ..
sudo chgrp -R www-data zwinode
sudo chmod -R g+w zwinode
```
This should work for Ubuntu. For other Linux flavours, Apache may use different ownership (not  www-data).

Then navigate the browser to the URL of the directory "zwinode" (e.g. http://localhost/zwinode). The server will check your installation and pre-install a few ZWI files in the directory "ZWI".
Note that cookies in the web browser should be enabled.
For the final deployment, the "zwinode" directory can be renamed.

## Testing

Navigate your browser to the URL from earlier. You will see the search form. Search your locally stored ZWI and read them.
Search for the word "encycloreader". If you can find it, the installation is successful.

Then go to the menu "Edit" and click "ZWI sharing point". This link shows how many ZWI files are installed.


## Hosting ZWI articles

To host articles from similar ZWINode sites, or specialized Encyclosphere nodes, such as [encycloreader.org](https://encycloreader.org) or [encyclosearch.org](https://encyclosearch.org),
which are supported by the KSF, go to the menu "Edit" and login using the top-right button. 
This enables options to host external ZWI using the URL path. 

One can also upload locally stored ZWI files. In addition, one can copy the entire publishers 
using the command-line tools. All of this is explained in the "Edit" page.  

## Removing articles

To remove an article, you should login first. Then go to the page "Articles". You will see small crosses near the article's numbers. Click on the cross.
Then you can press "Yes" to remove the article.

## How to upgrade

During upgrade you must preserve the directory "ZWI". Go to the directory above the ZWINode
installation and run:

```
git clone https://gitlab.com/ks_found/zwinode.git
cd zwinode
git clone https://gitlab.com/ks_found/ZWINetwork.git
```

Then go to the menu "Edit" and press "Reindex repository".
You can also re-index the "ZWI" directory manually as explained below.

## Doing everything manually 

Theses steps are only needed if you want to perform the installation manually:

```
git clone https://gitlab.com/ks_found/zwinode.git
cd zwinode 
git clone https://gitlab.com/ks_found/ZWINetwork.git
source ZWINetwork/zwi_network.sh
```
Now we make a mirror of ZWI files. We download a few ZWI files from a test repository:  

```
zwi_get -i https://encycloreader.org/ZWINODE/ZWI/ 
./index.sh
```

The first command will download articles to the directory "ZWI", and the second command will index them. We use the source encycloreader.org, but any source should be possible. Read [ZWINetwork documentation](https://gitlab.com/ks_found/ZWINetwork).

After this step, your articles should be located in

```
[YOUR URL]/zwinode/ZWI/ 
```
where [YOUR URL] points to your current domain served from the directory /var/www/html/.

## Adding more articles from a remote publisher

If you want to make a node with many ZWI files from wikipedia or other publishers, use the full encycloreader.org repository: 

```
zwi_get -i https://encycloreader.org/db/ZWI/
./index.sh
```

The first command will start downloading the complete encycloreader.org repository (takes very long!). The second command will index article for export and for local search. 

Generally, if you would like to replicate articles from any remote node, follow these steps:

```
source ZWINetwork/zwi_network.sh
zwi_get -p [PUBLISHER] -i [URL]
zwi_index -i ZWI
python3 zwi_sqlite.py
```

where [PUBLISHER] and [URL] are publisher name and [URL]. Read [ZWINetwork documentation](https://gitlab.com/ks_found/ZWINetwork) for more commands. In this example we use 2 seprate indexing commands instead of the single "index.sh". 

It should be noted that the Apache web server may not allow to copy new files to the directory "ZWI". Therefore,
is it recommended to add the user to the Apache group:


```
usermod -a -G www-data (your username)
chgrp www-data  ZWI
chmod g+rwxs ZWI
```

## Adding your articles 

If you add new ZWI to the directory "ZWI/en", rebuild the index:

```
zwi_index -i ZWI
python3 zwi_sqlite.py
```

The first command creates index for public export of your articles. The second command creates SQLite3 file for local search.

## Finalizing  

Now your ZWI node is ready. If you want to make your site visible to others, submit its URL to https://encycloreader.org/sites/
(or share it with your friends). Other people with similar ZWInode sites can easily discover our articles and host them on their nodes. 

## License

GNU Lesser General Public License v3.0. https://www.gnu.org/licenses/lgpl-3.0.en.html
 
## Project status

 - Version 1.5, Feb 11, 2024 
 - Version 1.0, Oct 27, 2023  

