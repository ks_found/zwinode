<?php
/* Common styles */
require_once("config.php");
############## do not change below #################
function footer(){
  global $conf; 
  $powered="Powered by ZWINode. Supported by <a href=\"https://encyclosphere.org/\"> KSF <img src='/img/150px-Encyclosphere_logo_image_alone_24.png' alt=\"Encyclosphere\" style='vertical-align:middle;margin:0;'/></a>";
  $sitename="<hr><br>".$conf['title'] ." : ".$conf['tagline'];
  return $sitename.". ".$powered;
}


?>

