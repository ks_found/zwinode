#!/bin/bash
# -*- coding: utf-8 -*-
# Initialize database for hit counter 
# @version 1.0. Nov 25, 2022
# S.V.Chekanov (KSF)


"exec" "python3" "-Wignore" "$0" "$@"

__version__ = '1.4'
__author__  = 'Sergei Chekanov  (chakanau@hep.anl.gov)'
__doc__     = 'Make a file with input ROOT files'



import re,sys,os
import sqlite3
from time import time
import tempfile
import zipfile

# Max number of ZWI files. Add -1 for all files
NMaxNumber=-1

DB="edit_ksf.sqlite3"
script_dir=os.path.dirname(os.path.realpath(__file__))
print("From = ",script_dir)

os.system("rm -f " + DB);

conn = sqlite3.connect(DB) # or use :memory: to put it in RAM
cursor = conn.cursor()

cursor.execute("PRAGMA foreign_keys=OFF;")
conn.commit()
cursor.execute("""PRAGMA encoding = "UTF-8";""")
conn.commit()

table="edit"
print("Created table=",table)
sql="CREATE TABLE '"+table+"' ('title' VARCHAR(255), 'hash_ip' VARCHAR(255), 'publisher' VARCHAR(255), 'timestamp' INTEGER);"
cursor.execute(sql)
conn.commit()

conn.close()
print("Written=",table,"table")
print("Created=",DB)

os.chmod(DB, 0o0777)
