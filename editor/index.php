<!DOCTYPE html>

<?php
require_once("../config.php");
require_once("../comstyle.php");
?>

<html >
   <head>
       <title>Add article to <?php echo $conf['title'];?> </title>
       <meta charset="UTF-8">
       <meta name="description" content="Add article to EnHub">
       <meta name="keywords" content="ZWI, Article, Edit">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>

<style>
.navbar-brand {
  color: #85c1e9;
}
</style>


</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../index.php">ZWINode</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

        <li class="nav-item">
          <a class="nav-link" href="../articles/">Articles</a>
        </li>
       <li class="nav-item">
          <a class="nav-link active" href="index.php">+Add</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../edit/">Edit</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../about/">About</a>
        </li>


      </ul>
    </div>
  </div>
  <!-- Navbar content -->
</nav>


<?php
require_once("../config.php");
require_once("../common.php");

$mess = <<<'HEREA'
<div class="container">
<h2>Creation of articles is not allowed</h2>

<div style="margin-top:2px; float:center;  top:40px; font-size:14px;">
<button class="hwbutton" onclick="location.href='../edit/index.php?login=0';">Login</button>
</div>

<p>
</p>

This message indicates that the ZWINote was not configured to publish article by anonymous people.
Only the owner of this ZWINode is allowed to create articles. Please login and return to this page again.

</div>


</body>
</html>

HEREA;


if ($conf['create_article'] == 0) {

$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
if (!$isPasswordCorrect) {
    die($mess);
};


};

?>




<div class="container">

<h2>Create a new article:</h2>

<form action="form_create.php"  class='author-form' method="POST">

<label for="ftitle">Title of your new article:</label><br>
<input type="text" name="ftitle" id="ftitle" size="90%" />

<p>
</p>

<label for="fname">Your name (optional):</label><br>
<input type="text" name="fname" id="fname" size="50px"/>
<p>
</p>

<input type="submit" name="create" value="Create" />
</form>

<p>
</p>

Submission of this form creates a secure URL link to the draft of your article.
Do not share the unique URL unless you plan to work on this draft together.

<p>
</p>

</div>

<footer class="bg-white text-center">
    <p><?php echo footer()  ?>   </p>
</footer>


</body>
</html>
