<?php

// SQL database
require_once("common.php");
require_once("zwimaker.php");

$title ="";
if (isset($_POST['title']))  $title =$_POST['title'];
$title = trim($title);

$topic ="";
if (isset($_POST['topic']))  $topic =$_POST['topic'];
$topic = trim($topic);

$author ="";
if (isset($_POST['author']))  $author =$_POST['author'];
$author = trim($author);

$article ="";
if (isset($_POST['article']))  $article =$_POST['article'];
$article = trim($article);

$zwiname ="";
if (isset($_POST['zwiname']))  $zwiname=$_POST['zwiname'];
$zwiname = trim($zwiname);

$path ="";
if (isset($_POST['path']))  $path =$_POST['path'];
$path = trim($path);

$zipfilename ="";
if (isset($_POST['zipfilename']))  $zipfilename =$_POST['zipfilename'];
$zipfilename = trim($zipfilename);


$yes_no ="";
if (isset($_POST['yes_no']))  $yes_no =$_POST['yes_no'];

if($yes_no == "no") {
       // header("Location: " . $newlink);
        die("Sorry, we do not allow submissions to Encyclosphere without publishing your article in https://enhub.org.");
}


if (strlen($path)<1) die("No path is given");
if (strlen($article)<1) die("No article is given");
if (strlen($zwiname)<1) die("No ZWI name is given");
if (file_exists($zipfilename)==false) die("No ZWI file was created!");

# echo $zipfilename;

$zwifile = basename($zipfilename); 

echo "<br>";

$hostORIP=$_SERVER['HTTP_HOST'];
if (is_ip($hostORIP)) {
  die("Error. Your IP is " . $hostORIP . " We cannot create  a publisher for ZWI library using your IP address. Solution: use this software on a server with some domain name.");
}


define('ROOTPATHC', __DIR__);
$ZWIPATH=dirname(ROOTPATHC);
$newdir=$ZWIPATH . "/ZWI/en/zwinode/" . $hostORIP  . "/"; 
$newfile=$newdir . $zwifile;

# echo $newfile;

if ( !is_writable( $ZWIPATH )) {
    die("<h3 style=\"color: red;\">The directory \"$ZWIPATH\" mist be writable by the server! Upload is not allowed</h3></html></body>");
}

if (createPath($newdir) == false){
      die("Error in creating a directory $newdir. Probably, you just use some IP address without the domain name. We are not allowing to created a publisher name using your IP address. Solution: use this software on a server with some domain name");
};


if (copy($zipfilename,  $newfile)) {
	
     $command ="../index.sh index > ../tmp/index.log 2>&1";
     $output = shell_exec($command);

     if (file_exists($newfile)) {  
       $getLastModDir = filemtime( $newfile );
       $getsize = filesize( $newfile ); 
       $command ="python3 ../edit/zwi_sqlite_add.py $newfile $getLastModDir $getsize  >> ../tmp/index.log 2>&1";
       $output = shell_exec($command);
     }

     //echo "<pre>".$output."</pre>";
     //echo nl2br("<pre>".$output."</pre>");
     //$output = shell_exec('grep Total install.log');
     //echo "<pre>".$output."</pre>";
     echo "<p></p><h4>The ZWI file was added <font color=\"green\">OK</font></h4>";
     echo "<form action=\"../articles/index.php\" method=\"post\" > <button type='submit'>Check your article</button> </form>";
     die();
	
} else {

    $errors= error_get_last();
    echo "RENAME ERROR: ".$errors['type'];
    echo "<br />\n".$errors['message'];
    die("Moving failed.. Exit");
	
	
	
};

$str = <<<EOD
<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>ZWI submit</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, minimum-scale=0.25, maximum-scale=5.0"/>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
</head>
<body>
<p>
</p>
<center>
<h2>The article was published <a href="../articles/">here</a>
</h2>
<br>
</html>
EOD;
print($str);


?>

