<?php
// S.Chekanov (KSF)
// Create ZWI file from inputs 

require_once("Html2Text.php");
require_once("ShortDescription.php");
require_once("common.php");



/**
 *  $title - title of article
 *  $author - author of article
 *  $article_html - string with HTML of article
 *  $filepath - path to ZWI file
 *  $zwiname -random string to make name unique
 *  $topic - topic 
 *  @return ZWI file name if it was created.
 *  **/
function makeZWI($title, $author, $article_html, $filepath, $zwiname='', $topic='') {

  global $wgMzwiLicense, $wgMzwiName, $Lang, $MAIN_URL;



  $tmp="";
  // create ZWI file

  if (strlen($title)<2) return $tmp; 

  $xfile=$title; 
  //$xfile=first7words($title,7)."_".$zwiname;
  //$zipfilename=$filepath . "/" . normalizeString($xfile) . ".zwi";;
  $zipfilename=$filepath . "/" . sanitizeFileName($xfile) . ".zwi";


   // preserver revision
   $timestamp=time(); 
   $revision = $filepath . "/article_".$timestamp .".html";
   file_put_contents($revision, $article_html);


  // remove old
  if (file_exists($zipfilename)==true) unlink($zipfilename);

   $zip = new ZipArchive();
   if ($zip->open($zipfilename, ZipArchive::CREATE)!==TRUE) {
   exit();
    }

   $html2TextConverter = new \Html2Text\Html2Text($article_html);
   $articleTXT = $html2TextConverter->getText();

   // get description
   $DESC = new ShortDescription(null,$articleTXT);
   $description=$DESC->getDescription();

        $zwiImages= array();
        $img_replace = array();
	// extract images	
	$doc = new DOMDocument();
        $doc->loadHTML($article_html);
        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $tag) {
          $imagepath_array= array(); 
	  $imagepath_array[]=$tag->getAttribute('src');
	  //die("Found images.." . count($imagepath_array) );
	  //collect all images.. 
	  foreach ($imagepath_array as $imagepath) {
          if ($imagepath != null) {		  
	   if (strlen($imagepath)>1) {
	      $full_image_path=$filepath . "/" . $imagepath;
              //print($full_image_path);
	      $img = basename($imagepath);
              if (file_exists($full_image_path)) {
                 $in_img="data/media/images/".$img; 
                 $zip->addFile($full_image_path,  $in_img);
	         $zwiImages[$in_img] = sha1_file($full_image_path);
                 $img_replace[$imagepath]=$in_img;
               }
            }
           }
	  } // end of possible src sets 
	} // end image loops 



	 // correct image src links
         foreach ($img_replace as $key => $value) {
                     $article_html = str_replace($key, $value, $article_html);
		     //echo "{$key} => {$value} ";
         }

	 //die("Image are done");

	if (file_exists("data/html_header.html") == false) { 
		die("Cannot find header, footer and css styles to add to HTML!"); 
	};

        // add styles
        $zip->addFile('data/common.css',    "data/css/common.css");
	$zip->addFile('data/darkmode.css',  "data/css/darkmode.css");
        $zip->addFile('data/design.css',    "data/css/design.css");
        $zip->addFile('data/poststyle.css', "data/css/poststyle.css");
 
	$zwiImages['data/css/darkmode.css'] = sha1_file('data/darkmode.css');
        $zwiImages['data/css/design.css'] = sha1_file('data/design.css');
        $zwiImages['data/css/common.css'] = sha1_file('data/common.css');
        $zwiImages['data/css/poststyle.css'] = sha1_file('data/poststyle.css');

        $article_header = file_get_contents("data/html_header.html");
        $article_footer = file_get_contents("data/html_footer.html");

	$article_html=$article_header . $article_html . $article_footer;

       // all images: $allimages
       $zip->addFromString("media.json", json_encode($zwiImages,JSON_PRETTY_PRINT));
       $zip->addFromString("article.html", $article_html);



                        $creator_name=array();
                        $creator_name[]=$author;

                        $revisions_array=array();
                        $topic_array=array($topic);

                        $content_array= array();
                        $content_array["article.html"]=sha1($article_html);
                        $content_array["article.txt"]=sha1($articleTXT);
                        $tt=array();
                        $tt['ZWIversion'] = 1.3;
                        $tt['Primary'] = "article.html";
                        $tt['Title'] =$title; 
                        $tt['ShortTitle'] =""; // $title;
                        $tt['Topics'] =$topic_array;
			$tt['Content'] = $content_array;
                        $tt['Publisher']=$wgMzwiName;
			$tt['TimeCreated'] = $timestamp;
                        $tt['LastModified'] = $timestamp;
                        $tt['Revisions'] = $revisions_array;
			$tt['UserName'] = $author; 
                        $tt['CreatorNames'] = $creator_name;
                        $tt['RevisionsCount'] = 1;
			$tt['ContributorNames'] = "";
                        $tt['GeneratorName'] = "EnHub";
                        $tt['SourceURL'] = "https://enhub.org";
                        $tt['Lang'] = $Lang;
			$tt['Comment'] = "";
                        $tt['Rating'] = array("0","0");
                        $tt['License'] = $wgMzwiLicense;
                        $tt['Description']=$description;
                        $tt['Author']=$author;
                        $tt['PublicationDate']=$timestamp;
                        $tt['SupportDeclarations']=array();
			$tt['SourceURL']= $MAIN_URL . "/articles/"; 
	              	// add metadata
                        $zip->addFromString("metadata.json", json_encode($tt,JSON_PRETTY_PRINT));


                         // add revisions
			$files = glob($filepath . "/article_*.html");
                        foreach ($files as &$value) {
                                $arch = basename($value); 
				$zip->addFile($value,  "data/attic/". $arch);	
		         }


			// close file
			$zip->close();



			return $zipfilename;

}



?>

