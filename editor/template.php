<!DOCTYPE html>

<html >
    <head>
       <title>Draft article</title>
       <meta charset="UTF-8">
       <meta name="description" content="ZWINode article editor">
       <meta name="keywords" content="ZWINode, Editor, ZWI">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="noindex">
       <link REL="SHORTCUT ICON" HREF="../../../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../../../favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" type="text/css" href="../../../css/style.css"/>
       <link rel="stylesheet" type="text/css" href="../../css/encyclosphere.css"/>

<style>
#div-with-bg {
        margin-top:20px;
        text-align: center;
}

.tox .tox-promotion .tox-promotion-link {
        display:none;
}

.etitle {
  margin-top: 10px;
  width: 80%;
  padding: 2px 2px;
  box-sizing: border-box;
  border: 0px;
  font-size: 1.6em;
  font-family:Helvetica,Arial,sans-serif;
  font-weight: bold;
  line-height: 1.3; 
  min-width: 200px;
}

.epublisher {
  line-height: 1.3; 
  width: 80%;
  padding: 2px 2px;
  border: 0px;
  font-size: 1.0em;
  min-width: 480px;
  font-family:Helvetica,Arial,sans-serif;
  font-size: 1em;
  min-width: 200px;
  margin-bottom:10px;
}

#header img {
  vertical-align: middle;
  margin-bottom: 8px;
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 200px;
}

.navbar-brand {
  color: #85c1e9;
}

</style>


<script src="../../tinymce62/js/tinymce/tinymce.min.js" referrerpolicy="origin"></script>


<script>
var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

        // Insert an image in the editor at the cursor position
        // Function required for Bludit
        function editorInsertMedia(filename) {
                tinymce.activeEditor.insertContent("<img src=\""+filename+"\" alt=\"\">");
        }

        // Returns the content of the editor
        // Function required for Bludit
        function editorGetContent() {
                return tinymce.get('textarea#encycloreader').getContent();
        }


tinymce.init({
  selector: 'textarea#encycloreader',
  plugins: 'print preview paste importcss searchreplace autolink link autosave save directionality code visualblocks visualchars fullscreen image media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
  external_plugins: {'mathjax': '/editor/extra/tinymce-mathjax/plugin.min.js'},
  mathjax: {
                lib: '/editor/extra/MathJax/es5/tex-mml-chtml.js', //required path to mathjax
                symbols: {start: '\\(', end: '\\)'}, //optional: mathjax symbols
                className: "math-tex", //optional: mathjax element class
                configUrl: '/editor/extra/tinymce-mathjax/config.js' //optional: mathjax config js
            },
  imagetools_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | formatselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify outdent indent |  numlist bullist | code mathjax |  link unlink image  code | pagebreak | fullscreen  preview |  template anchor codesample | forecolor backcolor removeformat',
  toolbar_sticky: true,
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  importcss_append: true,
  height: "480", 
  branding: false,
  browser_spellcheck: true,
  menu: {
    file: { title: 'File', items: ' preview | export print | deleteallconversations' },
  },

  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  toolbar_mode: 'sliding',
  contextmenu: false,
  skin: useDarkMode ? 'oxide-dark' : 'oxide',
  content_css: "/editor/css/encyclosphere.css",
  content_style: '.math-tex {font-size: 1em;}',
  image_title: true,
  // enable automatic uploads of images represented by blob or data URIs
  automatic_uploads: true,
  // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
  images_upload_url: 'postAcceptor.php',
  // here we add custom filepicker only to Image dialog
  file_picker_types: 'image',
  // and here's our custom image picker
  image_advtab: true,
  file_picker_callback: function(cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');

    // Note: In modern browsers input[type="file"] is functional without
    // even adding it to the DOM, but that might not be the case in some older
    // or quirky browsers like IE, so you might want to add it to the DOM
    // just in case, and visually hide it. And do not forget do remove it
    // once you do not need it anymore.

    input.onchange = function() {
      var file = this.files[0];

      var reader = new FileReader();
      reader.onload = function () {
        // Note: Now we need to register the blob in TinyMCEs image blob
        // registry. In the next release this part hopefully won't be
        // necessary, as we are looking to handle it internally.
        var id = 'imageID' + (new Date()).getTime();
        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        // call the callback and populate the Title field with the file name
        cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };

    input.click();
  }
	
	
	
});

</script>


</head>

<body>

<ul class="topnav">
       <li class="nav-item">
          <a class="nav-link" href="../../../articles/">Articles</a>
        </li>
       <li class="nav-item">
          <a class="nav-link active" href="../../index.php">+Add</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../../../edit/">Edit</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../../../about/">About</a>
        </li>
</ul>


<div class="container">


<?php

REPLACEMENT1

$filename='./article.html';
$article = "";
if (file_exists($filename)) {
   $article = file_get_contents($filename);
}

if (isset($_POST["content"])) {
        $content=$_POST['content'];
        file_put_contents($filename, $content);
}

$rev="rev ".date('m/d/Y H:i', $time );
if (isset($_GET["rev"])) {
        $rev=$_POST['rev'];
        $rev="rev ". date('m/d/Y H:i', $rev );
}


$txt = <<<EOD


<div style="margin-top:4px;width:150px;float:right;margin-right:10px;margin-bottom:5px;background-color:white;">
<div style="width:210px;height:18px;padding:3px;border:2px solid gray;margin-left:2px;text-align:center;font-family: monospace; font-size:14px; position: relative; float:right; background-color:orange; ">
$rev
</div>

</div>


<!-- start main form -->
<form method="post" action="article.php">

<input type="hidden" id="zwiname" name="zwiname" value="$zwiname">

<textarea class="etitle" id="etitle" name="etitle" rows="1">
$title
</textarea>

<p></p>
<textarea class="epublisher" id="epublisher" name="epublisher" rows="1">
$publisher
</textarea>

<input type="submit" name="submit" value="Save" style="width:85px;margin-top:-10px;margin-bottom:6px;float:right;margin-right:5px;">


<textarea id="encycloreader" name="content">
$article
</textarea>
</form>
EOD;

echo $txt;
?>


</div>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
<script type="text/x-mathjax-config">
MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  }
};
</script>


</body>
</html>
