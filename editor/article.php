<!DOCTYPE html>

<html >
    <head>
       <title>View article</title>
       <meta charset="UTF-8">
       <meta name="description" content="ZWINode article viewer">
       <meta name="keywords" content="Article, Editor, Encyclosphere">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="noindex"> 
       <link REL="SHORTCUT ICON" HREF="../../../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../../../favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" type="text/css" href="../../../css/style.css"/>
       <link rel="stylesheet" type="text/css" href="../../css/encyclosphere.css"/>
</head>

<body>

<ul class="topnav">
  <li><a href="../../../index.php">Home</a></li>
  <li><a href="../../../articles/">Articles</a></li>
  <li><a href="../../../editor/index.php">+ Add</a></li>
  <li class="right"><a href="../../../about.php">About</a></li>
</ul>


<div class="container">

<?php

require_once("../../common.php");
require_once("../../htmlcleaner.php");

$zwiname="";
if ( isset($_POST['zwiname']) ){
      if (strlen($_POST['zwiname']) >0) {
              $zwiname=trim($_POST['zwiname']);
        };
}

$path=getcwd();
$chunks = explode('/', $path);
$nlen=count($chunks);
$newlink=$chunks[$nlen-2] . "/" .  $chunks[$nlen-1] . "/index.php";
$backlink="/editor/" . trim($newlink);

// Z-pin of article
$zpin=$chunks[$nlen-1];

$txt = <<<EOD
<!-- start right  box -->
<div style="margin-top:-2px; float:right; width:140px;">

<button class="readerbutton" onclick="location.href='index.php';">Back to Editor</button>
<!--
<div style="width:120px;height:18px;padding:2px;border:2px solid gray;margin-right:10px;text-align:center;font-family: monospace; font-size:16px; position: relative; float:right; background-color:orange; margin-top:6px; ">
$zpin
</div>
-->
</div>
<!-- end right box  -->
EOD;

print($txt);

$title="";
if ( isset($_POST['etitle']) ){
      if (strlen($_POST['etitle']) >0) { 
              $title=trim($_POST['etitle']); 
	      print("<h1>".$title."</h1>");
        }; 
}


$author="";
if ( isset($_POST['epublisher']) ){
	if (strlen($_POST['epublisher']) >0) {
            $author=trim($_POST['epublisher']);
	    print("<i>by ".$author."</i></br>");
	}
}

$html="";
if ( isset($_POST['content']) ){
   //This is what you want - HTML content from tinyMCE
   //just treat this a string
   $html=trim($_POST['content']);   
   //clean 
   //$html = replaceTags($html, $replace_tags);
   //$html = stripTags($html, $remove_tags);
   //$html = stripAttributes($html, $remove_attribs);
	
   if (save_html_to_file($html, './article.html') == false ){
      header('Location:index.php');
   }
}

// only for long
if (strlen($html)>30) {
  $cmd="hunspell -l article.html > article.err &";  
  $output = shell_exec($cmd);
}


/**
 * 
 * @param string $content HTML content from TinyMCE editor
 * @param string $path File you want to write into
 * @return boolean TRUE on success
 */
function save_html_to_file($content, $path){
   return (bool) file_put_contents($path, $content);
}

$topiclist=getTopicList();


$txt = <<<EOD
<hr>
<div style="width:100%; min-height:380px;">
$html
</div>
<hr>

<form action="../../form_check.php" method="POST" style="float:right; display:block;margin-top:10px;margin-bottom:10px;">


<!-- start right  box -->
<div style="margin-top:-2px; float:right; width:250px;">

<select class="topicbox" name="topic" id="topic"  style="float:left;position:relative;margin-top:0px;padding:3px;margin-right:6px;">
  $topiclist;
</select>

<input type="submit" name="zwisubmit" value="Continue" style="float:right;position:relative;" />

</div>
<!-- end right  box -->

<input type="hidden" id="zpin" name="zpin" value="$zpin">
<input type="hidden" id="author" name="author" value="$author">
<input type="hidden" id="title" name="title" value="$title">
<input type="hidden" id="article" name="article" value="article.html">
<input type="hidden" id="zwiname" name="zwiname" value="$zwiname">
<input type="hidden" id="path" name="path" value="$path">
</form>
EOD;
print($txt); 
?>

</div>


<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
<script type="text/x-mathjax-config">
MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  }
};
</script>

<!--
<div class="footer">
  <p>&copy; EnHub article editor</p>
</div>
-->

</body>

</html>
