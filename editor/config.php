<?php
// S.Chekanov. Main configuration file.

// Main URL 
$MAIN_URL="https://enhub.org";

// Minimum length of articles
$MinLengthArticle=30;


// Fraction of misppelled words to accept
$PercentageOfMisspell=0.30;

// list of topics
$Topic_list = array("Art", "Culture", "Companies", "Music", "Movies", "Software", "Nature", "Games", "Technology","Science","Religion","History", "Places","Politics","Philosophy");


################### ZWI creation ##################
# what about the license?
$wgMzwiLicense='CC BY-SA 3.0';
# publsiher
$wgMzwiName="ZWINode";
#
$Lang="en";
#######################################################

?>

