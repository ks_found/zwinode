<!DOCTYPE html>

<?php
require_once("../config.php");
require_once("../comstyle.php");
?>

<html >
    <head>
       <title>Revisions of <?php echo $conf['title'];?></title>
       <meta charset="UTF-8">
       <meta name="description" content="List of articles from <?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <style type="text/css" title="currentStyle">

<style>
.navbar-brand {
  color: #85c1e9;
}

body {
  margin: 0;
  font-size: 16px;
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
}


.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #85c1e9;
}

h1 {
font-size: 32px;
font-weight: 600;
line-height: 1.4;

}

h2 {
font-size: 26px;
font-weight: 600;
line-height: 1.4;
margin-top: 10px;
color: #6495ED; 
}

pre {
  font-size: 1em;
  border: 2px solid grey;
  width: 450px;
  border-left: 10px solid #6495ED;
  border-radius: 5px;
  padding: 14px;
}


</style>


    </head>
    <body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../"><?php echo $conf['title'];?></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

        <li class="nav-item">
          <a class="nav-link active" href="../articles/">Articles</a>
        </li>

       <li class="nav-item">
          <a class="nav-link" href="../editor/">+Add</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="../edit/">Edit</a>
        </li>

	<li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search"> 
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">

<H2>Revisions of the articles</H2>

<?php 

require_once("../common.php");

$fzwi ="";
if (isset($_GET['fzwi']))  $fzwi =$_GET['fzwi'];
$fzwi=trim($fzwi);


$isPasswordCorrect = false;
if(isset($_COOKIE['zwinode']))
        $isPasswordCorrect = password_verify( $conf['password'], $_COOKIE["zwinode"]);
if (!$isPasswordCorrect) {
    die("You did not login to process this action");
};


//die($fzwi);
if (strlen($fzwi)==12) {

	$files = glob("draft/".$fzwi ."/article_*.html");
        rsort($files);
        $nmax=count($files);


	// edit
        $html="<form action=\"form_edit.php\" method=\"get\">\n"; 
        $html = $html .  "<input type=\"hidden\" id=\"fzwi\",  name=\"fzwi\" value=\"" . $fzwi . "\">\n";
	$html = $html . "<label for=\"revision\">Select a revision:</label>\n<select id=\"rev\" name=\"rev\">";
	$nn=0;
        $select="";
	foreach ($files as &$value) {
                 $value = str_replace(".html", "", $value);
		 $pieces = explode("/", $value);
                 $key=$pieces[2]; 
                 $xd=explode("_", $key);
                 $sdate=""; 
                 $xvalue="";
		 if (count($xd)>1) {
                         $nmax =  $nmax -1;       
 			 $sdate=date('m/d/Y H:i', $xd[1] );
                         $xvalue=$xd[1];
			 if ($nn==0)
				  $select=$select . "<option value=\"" . $xvalue  . "\"> ".  "rev " . $nmax+1 ." (" . $sdate .") current </option>";
                         else     $select=$select . "<option value=\"" . $xvalue  . "\"> ".  "rev " . $nmax+1 ." (" . $sdate .") </option>";

			 $nn++;
		 };
	 }
          $html=$html . $select . "</select> <button class=\"hwbutton\" type=\"submit\">Edit this article</button> </form>";
	  echo $html;


 
         // compare
        $html="<h2>Compare revisions</h2><form action=\"form_diff.php\" method=\"get\">\n";
        $html = $html .  "<input type=\"hidden\" id=\"fzwi\",  name=\"fzwi\" value=\"" . $fzwi . "\">\n";
        $html = $html . "<label for=\"revision1\"> New revision:</label>\n<select id=\"rev1\" name=\"rev1\">" . $select . "</select>";
        $html = $html . "&nbsp; <label for=\"revision2\"> Old revision:</label>\n<select id=\"rev2\" name=\"rev2\">" . $select . "</select>";
	$html=$html .  "&nbsp; <button class=\"hwbutton\" type=\"submit\">Compare</button> </form>";
	echo $html;
 





/*
	$draft="draft/".$fzwi . "/index.php";

        if (file_exists($draft)  == true) {
          header('Location: '. $draft );
        } else {
           die("We cannot find " . $fzwi . "  draft");
        }
*/

} // end draft mode




?>

<p>
</p>


</div>

<footer class="bg-white text-center">
    <p><?php echo footer()  ?>   </p>
</footer>



</body>
</html>
