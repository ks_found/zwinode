<!DOCTYPE html>

<?php
require_once("../config.php");
require_once("../comstyle.php");
?>

<html >
   <head>
       <meta charset="UTF-8">
       <title>Revision difference.  <?php echo $conf['title'];?> </title>
       <meta name="description" content="List of articles from <?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, Encyclosphere, Publishing, Blogs, Articles">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">
       <link rel="apple-touch-icon" href="favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" href="diff/styles.css" type="text/css" charset="utf-8"/>
       <link rel="stylesheet" href="../css/style.css" type="text/css" charset="utf-8"/>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="../"><?php echo $conf['title'];?></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">

        <li class="nav-item">
          <a class="nav-link active" href="../articles/">Articles</a>
        </li>

       <li class="nav-item">
          <a class="nav-link" href="../editor/">+Add</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="../edit/">Edit</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="../about/">About</a>
        </li>
      </ul>
      <form class="d-flex" role="search" action="../find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search ..." aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
  <!-- Navbar content -->
</nav>

<div class="container">

<?php

// SQL database
require_once("./common.php");
require_once("Html2Text.php");

$fzwi ="";
if (isset($_GET['fzwi']))  $fzwi =$_GET['fzwi'];
$fzwi=trim($fzwi);


$revision1 ="";
if (isset($_GET['rev1']))  $revision1 =$_GET['rev1'];
$revision1=trim($revision1);

$revision2 ="";
if (isset($_GET['rev2']))  $revision2 =$_GET['rev2'];
$revision2=trim($revision2);

//die($revision1 ." vs ".  $revision2 . " Not implemented!");

$URL="location.href='rev.php?fzwi=" . $fzwi ."';";
//die($URL);
$extrabutton = <<<EOD
<div style="margin-top:50px; position: absolute; right:200px; top: 15px; font-size:14px;">
<button class="hwbutton" onclick="$URL">Back</button>
</div>
EOD;

print($extrabutton);

print("<H3>Difference between the revision " . $revision1 . " and " .  $revision2 ." </H3>");


//die($fzwi);

$file1="";
$file2="";
if (strlen($fzwi)==12) {
        //$draft="draft/".$fzwi . "/index.php";
	// only care about dates, not current
	if (strlen($revision1)==10 && strlen($revision2)==10) { 
                 $file1="draft/".$fzwi . "/article_".$revision1.".html";
                 $file2="draft/".$fzwi . "/article_".$revision2.".html";	  
	};
} // end draft mode

//die($file1);
//print(file_get_contents( $file2 ));

require_once dirname(__FILE__).'/diff/php-diff/lib/Diff.php';
require_once dirname(__FILE__).'/diff/php-diff/lib/Diff/Renderer/Html/SideBySide.php';
//require_once dirname(__FILE__).'/diff/php-diff/lib/Diff/Renderer/Html/Inline.php';

//require_once 'php-diff/lib/Diff.php';
//require_once 'php-diff/lib/Diff/Renderer/Html/SideBySide.php';

//ie(dirname(__FILE__).'/diff/php-diff/lib/Diff.php');

// Include two sample files for comparison
//print($file1 . " " . $file2);

//$a = file_get_contents( $file1 );
//$b = file_get_contents( $file2 );

//$a = explode("\n", file_get_contents( $file1 ));
//$b = explode("\n", file_get_contents( $file2 ));

//print($file1."<br>"); 
//print($file2); 

//$a = explode("\n", file_get_contents(dirname(__FILE__).'/diff/a1.txt'));
//$b = explode("\n", file_get_contents(dirname(__FILE__).'/diff/a2.txt'));

$options2 = array(
        'do_links' => 'inline', // 'none'
                                // 'inline' (show links inline)
                                // 'nextline' (show links on the next line)
                                // 'table' (if a table of link URLs should be listed after the text.
                                // 'bbcode' (show links as bbcode)
        'width' => 40,          //  Maximum width of the formatted text, in columns.
                                //  Set this value to 0 (or less) to ignore word wrapping
                                //  and not constrain text to a fixed-width column.
    );


$html1=file_get_contents(dirname(__FILE__). "/" . $file1);
$html2=file_get_contents(dirname(__FILE__). "/" . $file2);
$html2TextConverter1 = new \Html2Text\Html2Text($html1, $options2);
$aa=$html2TextConverter1->getText();
$aa=preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $aa);
$a = explode("\n", $aa );

$html2TextConverter2 = new \Html2Text\Html2Text($html2, $options2);
$bb=$html2TextConverter2->getText();
$bb=preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $bb);
$b = explode("\n", $bb );
//echo $a;
//echo "<h2></h2>";
//echo $b;

//$a = explode("\n", file_get_contents(dirname(__FILE__). "/" . $file1));
//$b = explode("\n", file_get_contents(dirname(__FILE__). "/" . $file2));
//$a = file_get_contents( $file1 );
//$b = file_get_contents( $file2 );

//print_r($a);
//print_r($b);

// Options for generating the diff
$options = array(
    'ignoreWhitespace' => true,
    'ignoreCase' => false 
);

// Initialize the diff class
$diff = new Diff($a, $b, $options);
//$renderer = new Diff_Renderer_Html_Inline;
$renderer = new Diff_Renderer_Html_SideBySide;
$result=$diff->Render($renderer);
if (strlen($result)<10) {
print("<H3>No changes</H3>");
} else {
    print($result); 	
};


?>
        </div>


<footer class="bg-white text-center">
    <p><?php echo footer()  ?>   </p>
</footer>


	</body>
</html>

