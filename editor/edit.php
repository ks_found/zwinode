<!DOCTYPE html>
<html >
   <head>
       <title>Edit article</title>
       <meta charset="UTF-8">
       <meta name="description" content="Edit EnHub article">
       <meta name="keywords" content="ZWI, Article, Edit">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link REL="SHORTCUT ICON" HREF="../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
       <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>

<style>
.navbar-brand {
  color: #85c1e9;
}
</style>

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="/index.php">EnHub</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">
        <li class="nav-item">
          <a class="nav-link" href="/articles/">Articles</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/blog/">Blogs</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/editor/">Add</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page"  href="/editor/edit.php">Edit</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/mediawiki/">MWiki</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/dokuwiki/">DWiki</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about.php">About</a>
        </li>
      </ul>
    </div>
  </div>
  <!-- Navbar content -->
</nav>


<div class="container">

<h2>Edit existing article:</h2>


<form action="form_edit.php"  class='author-form' method="POST">
<label for="fzwi">7-character pin  or the URL of article:</label><br>
<input type="text" name="fzwi" id="fzwi"  size="90%" />
<p>
</p>
<input type="submit" name="create" value="Submit" />
</form>

<p>
</p>

If the article is draft and thus is staged in this editor, 
find it using the 7-character pin. If it has been submitted to the EncycloSphere network,
use the URL to the ZWI file located on any mirror. The URL should ends with the extension ".zwi".

<p>
</p>

<h3>Privacy notice</h3>

We are committed to protecting your right to privacy. This resource <i> does not</i> collect  personal information (like the IP address).

</div>


<div class="footer"> 
  <p>EnHub &copy; Decentralized Publishing Platform</p>
</div>


</body>
</html>
