<?php

// global parameters
require_once('config.php'); 

function is_ip( $ip = null ) {

    if( !$ip or strlen(trim($ip)) == 0){
        return false;
    }

    $ip=trim($ip);
    if(preg_match("/^[0-9]{1,3}(.[0-9]{1,3}){3}$/",$ip)) {
        foreach(explode(".", $ip) as $block)
            if($block<0 || $block>255 )
                return false;
        return true;
    }
    return false;
}

// create directory recursivley (if does not exists)
function createPath($path) {
    if (is_dir($path))
        return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    $return = createPath($prev_path);
    return ($return && is_writable($prev_path)) ? mkdir($path) : false;
}


// return HTML of topics
function getTopicList(){
        # list of topics
	global $Topic_list;
	sort($Topic_list);
	$tmp="<option value=''>Select Topic</option>\n";
        // Iterating through the product array
        foreach($Topic_list as $item){
            $tmp = $tmp . "<option value='$item'>$item</option>\n";
        }
	return $tmp;
}


// truncate string to 7 words
function first7words($s, $limit=7) {
    return preg_replace('/((\w+\W*){'.($limit-1).'}(\w+))(.*)/', '${1}', $s);
}


// limit text size
function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
 return $text;
}

function contains($needles, $haystack) {
    return count(array_intersect($needles, explode(" ", preg_replace("/[^A-Za-z0-9' -]/", "", $haystack))));
}


 // Making a temporary dir for unpacking a zipfile into
function tempdir() {
    $tempfile=tempnam(sys_get_temp_dir(),'');
    // tempnam creates file on disk
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}

/**
 * Create a short, fairly unique, urlsafe hash for the input string.
 * https://roytanck.com/2021/10/17/generating-short-hashes-in-php/
 */
function generate_id( $input, $length = 12 ){
        // Create a raw binary sha256 hash and base64 encode it.
        $hash_base64 = base64_encode( hash( 'sha256', $input, true ) );
        // Replace non-urlsafe chars to make the string urlsafe.
        $hash_urlsafe = strtr( $hash_base64, '+/-_', '0abc' );
        // Trim base64 padding characters from the end.
        $hash_urlsafe = rtrim( $hash_urlsafe, '=' );
        // Shorten the string before returning.

        // never start from -. Replace with 0
        //if ( substr( $hash_urlsafe, 0, 1 )  === "-")  {
        //             $hash_urlsafe = substr_replace($hash_urlsafe,"0",0,1);
        //             }

        return substr( $hash_urlsafe, 0, $length );
}


//$ss=generate_id("s0andbox/sandbox.org/glacial-man-in-ohio1");
//echo $ss;

// random string
function generateRandomString($length = 10) {
    // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz'; 
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// random pin
function generateRandomPin($length = 5) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/** 
    Lower case 
    Remove Break/Tabs/Return Carriage
    Remove Illegal Chars for folder and filename
    Put the string in lower case
    Remove foreign accents such as Éàû by convert it into html entities and then remove the code and keep the letter.
    Replace Spaces with dashes
    Encode special chars
**/
function normalizeString ($str = '')
{
    $str=  strtolower($str); 
    $str = strip_tags($str);
    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
    $str = strtolower($str);
    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = str_replace(' ', '-', $str);
    $str = rawurlencode($str);
    $str = str_replace('%', '-', $str);
    return $str;
}

function sanitizeFileName($dangerousFilename, $platform = 'Unix')
    {
        if (in_array(strtolower($platform), array('unix', 'linux'))) {
            // our list of "dangerous characters", add/remove
            // characters if necessary
            $dangerousCharacters = array(" ", '"', "'", "&", "/", "\\", "?", "*");
        } else {
            // no OS matched? return the original filename then...
            return $dangerousFilename;
        }

        // every forbidden character is replace by an underscore
        return str_replace($dangerousCharacters, '_', $dangerousFilename);
        }

?>

