<!DOCTYPE html>

<html >
    <head>
       <title>Error</title>
       <meta charset="UTF-8">
       <meta name="description" content="EnHub article viewer">
       <meta name="keywords" content="EnHub, Article, Editor, Encyclosphere">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="noindex"> 
       <link REL="SHORTCUT ICON" HREF="../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../favicon/apple-icon.png"/>
       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <link rel="stylesheet" type="text/css" href="../css/encyclosphere.css"/>
</head>

<body>

<ul class="topnav">
  <li><a href="../../../index.php">Home</a></li>
  <li><a href="../../index.php">Add article</a></li>
  <li><a href="../../edit.php">Edit article</a></li>
  <li class="right"><a href="about.php">About</a></li>
</ul>


<div class="container">
<center>

<?php

require_once("common.php");

$error_string=0;
if ( isset($_GET['e']) ){
     $error_string=$_GET['e'];
   }; 

$error = mb_substr($error_string, 0, 1); 
$zpin = substr($error_string, -7);

if ($error==1){
    print("<h2 style=\"color:red;\">Error</h2>This article is too short (less than " . $MinLengthArticle ." words). Keep working on it."); 
} else if ($error==2){
    print("<h2 style=\"color:red;\">Error</h2>This article contains images embedded into the HTML file.  You should upload the image files and link them to the HTML. Do not include online images with the source, picture and srcset tags. Clean up your HTML source."); 
} else if ($error==3){
    print("<h2 style=\"color:red;\">Error</h2>This article has very short or very long title.");
} else if ($error==4){
    print("<h2 style=\"color:red;\">Error</h2>Bad words are detected.");
} else if ($error==5){
    print("<h2 style=\"color:red;\">Error</h2>This article has too many grammatical mistakes (assuming English). Please correct it.");
}; 


print("\n<p></p><button class=\"readerbutton\" onclick=\"location.href='draft/".$zpin."/index.php';\">Back to Editor</button>\n");

?>


</center>
</div>



<div class="footer">
  <p>&copy; EnHub article editor</p>
</div>

</body>

</html>
