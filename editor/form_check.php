<?php
// Check article and asks further questions
// S.Chekanov
//
require_once("common.php");
require_once("zwimaker.php");


$title ="";
if (isset($_POST['title']))  $title =$_POST['title'];
$title = trim($title);

$author ="";
if (isset($_POST['author']))  $author =$_POST['author'];
$author = trim($author);

$article ="";
if (isset($_POST['article']))  $article =$_POST['article'];
$article = trim($article);

$zwiname ="";
if (isset($_POST['zwiname']))  $zwiname=$_POST['zwiname'];
$zwiname = trim($zwiname);

$path ="";
if (isset($_POST['path']))  $path =$_POST['path'];
$path = trim($path);

$topic ="";
if (isset($_POST['topic']))  $topic =$_POST['topic'];
$topic = trim($topic);

$zpin ="";
if (isset($_POST['zpin']))  $zpin =$_POST['zpin'];
$zpin = trim($zpin);

// backlink
$backlink="../../editor/draft/" . trim($zpin) . "/";


if (strlen($path)<1) die("No path is given");
if (strlen($article)<1) die("No article is given");
if (strlen($zwiname)<1) die("No ZWI name is given");

$err=0;

// title is too short?
$tlen=strlen($title);
if ($tlen<2 or $tlen>80){
  //  echo "<h3 style=\"color:red;\">Error: This article has very short or very long title.</h3>";
   $err=3;
}

// no author?
$tlen=strlen($author);
if ($tlen<2 or $tlen>80){
   $err=3;
}


$html = file_get_contents($path ."/" . $article);
$words=str_word_count($html,1);
$Nwords=count($words);

if ($Nwords<$MinLengthArticle){
     $err=1;
}

// first step!
if ($err>0) {
     $errurl = 'Location:error.php?e='.$err.$zpin;
     header($errurl);
     die();
};


// too many single characters
$ns=0;
$lower_words=array();
foreach ($words as &$v) {
    if (strlen($v)<2) $ns=$ns+1; 
}
if ( ($ns/$Nwords)>0.5 ) $err=5;


// check image
$pos1 = strpos($html, "data:image");
$pos2 = strpos($html, "base64");
if ($pos1 !== false) {
        if ($pos2 !== false) {
        $err=2;
        }
}

$pos1 = strpos($html, "source srcset");
if ($pos1 !== false) {
        $err=2;
        }


$array = array('porn', 'jerk', 'ass', 'shit', 'pussy', 'vagina', 'fuck','anal','blowjob','cock','anus','dick','jizz');
$strtolower = array_map('strtolower', $words);
$nw=count(array_intersect($strtolower, $array));
if ($nw>0)  $err=4;



$err_file=$path ."/article.err";
if (file_exists($err_file)) {
        $article_errors = file_get_contents($err_file); 
	$words_misspell=str_word_count($article_errors,1);
}

$err_count=count($words_misspell)/$Nwords; 
if ($err_count>$PercentageOfMisspell) {
      $err=5;
}

// second step!
if ($err>0) {
     $errurl = 'Location:error.php?e='.$err.$zpin;
     header($errurl);
     die();
};


//die($title);
$zipfilename=makeZWI($title, $author, $html, $path, $zwiname, $topic);
# Encyclosphere submission URL if select 1 
$permissionErr=0;
$chunks = explode('/', $zipfilename);
$nlen=count($chunks);
$short_name=$chunks[$nlen-3] . "/" .  $chunks[$nlen-2] . "/" .  $chunks[$nlen-1];  
$xsub="<input type=\"submit\" name=\"submit\" value=\"Submit to the Encyclosphere Network\" />";

$xmess="<h3>Article '" . $title ."' was created </h3>";
if (strlen($topic)>2) $xmess="<h3>Article '". $title ."' in the topic <i><font color=\"blue\">" . $topic ."</font></i> was created </h3>";

if (file_exists($zipfilename)==false) die("No ZWI file was created!"); 

//echo $zipfilename;

$str = <<<EOD
<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>ZWI submit</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, minimum-scale=0.25, maximum-scale=5.0"/>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
</head>
<body>
<p>
</p>
<center>
$xmess
<p>
</p>
<button class="readerbutton" type="submit" onclick="window.open('$short_name')">Download</button>
revision 1 of this article as a ZWI file. 

<p>
</p>

<form action="form_submit.php" method="post">
<div id="wrapper">
<label for="yes_no_radio">Do you agree to publish the article on this web site?</label>
<p>
<input type="radio" value="yes" name="yes_no" checked>Yes</input>
</p>
<p>
<input type="radio" value="no" name="yes_no">No</input>
</p>
</div>

<input type="submit" name="zwisubmit" value="Continue" />
<input type="hidden" id="author" name="author" value="$author">
<input type="hidden" id="title" name="title" value="$title">
<input type="hidden" id="article" name="article" value="article.html">
<input type="hidden" id="zwiname" name="zwiname" value="$zwiname">
<input type="hidden" id="path" name="path" value="$path">
<input type="hidden" id="zipfilename" name="zipfilename" value="$zipfilename">
<input type="hidden" id="topic" name="topic" value="$topic">
</form>



<center>
</body>
</html>
EOD;
print($str);


?>

