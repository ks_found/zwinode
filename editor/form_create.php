<?php

// SQL database
require_once("common.php");
require_once("config.php");

$title ="";
if (isset($_POST['ftitle']))  $title =$_POST['ftitle'];
$title = trim($title);

$publisher ="";
if (isset($_POST['fname']))  $publisher =$_POST['fname'];
$publisher = trim($publisher);

if (strlen($publisher)<1 or strlen($publisher)>80) die ( "Too short or too long publisher" );
if (strlen($title)<1 or strlen($title)>80) die("Too short or too long title");

# make directory if needed
if (!is_dir("draft/")) {
 mkdir("draft/", 0777, true);
 if (!is_dir("draft/")) die("The directory  \"editor/draft\" is not writable is not writable by the server!");
} 


// base URL
function baseHOST(){
    return $_SERVER['SERVER_NAME'];
}



$stitle = str_replace(' ', '_', $title);
$stitle=sanitizeFileName($stitle);
$hashpath=strtolower($wgMzwiName) . "/" . baseHOST() . "/" . $stitle;
//$article_hash=generateRandomString($length);
$article_hash=generate_id($hashpath);
$path = "draft/" . $article_hash;
if (!file_exists($path)) {
    mkdir($path, 0777, true);
}

// prepare directory for images
//$directs=$path . "/data/media/images";
$directs=$path . "/uploadAsset";
if (!file_exists($directs)) { 
  if (!mkdir($directs, 0777, true)) {
    die('Failed to create directory:' . $directs);
  }
};


$myfile = fopen($path."/index.php", "w") or die("Unable to open file!");

copy('article.php', $path.'/article.php');
copy('postAcceptor.php', $path.'/postAcceptor.php');

// random Pin for ZWI article
$zwiname=$hashpath; // generateRandomPin();

$txt = <<<EOD
\$title="$title"; 
\$publisher="$publisher"; 
\$zwiname="$zwiname";
\$articleID="$article_hash";
EOD;

//die($txt);


$template = file_get_contents('./template.php');
$newtxt = str_replace("REPLACEMENT1", $txt, $template);
fwrite($myfile, $newtxt);
fclose($myfile);

  /*
  // remove old
  $days30=60 * 60 * 24 * 30; // 
  $files = glob("draft/*");
  $now   = time();
  foreach ($files as $file) {
     if (is_dir($file)) {
       //print($file);     
       if ($now - filemtime($file) >= $days30) { // 30 days
         system('rm -rf -- ' . escapeshellarg($file), $retval); 
      }
    }
  }
 */

// base URL
function baseURL(){
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
        $url = "https://";
    }else{
        $url = "http://";
    }
    // Fix
    if(dirname($_SERVER['PHP_SELF']) == "/" || dirname($_SERVER['PHP_SELF']) == "\\") {
        return $url . $_SERVER['HTTP_HOST'];
    } else {
        return $url . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
    }
}


  $RED=baseURL() ."/" .  $path;
  header("Location: " . $RED);


?>

