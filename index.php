<!DOCTYPE html>

<?php

if (!file_exists("config.php")) {
$xerror = <<<EOD
<div style="font-size:1em; border: 3px solid #DE3103; box-shadow: 9px 6px 4px #5B2C0F; padding: 7px 11px; margin: 20px;">
The configuration file "config.php" does not exist. Copy the provided "config_sample.php" to "config.php" and make the needed changes. 
Then return to this URL.
<b>We exit now.</b> 
</div>
EOD;
die($xerror);
};

require_once("config.php");
?>


<html>
    <head>
       <title> <?php echo $conf['title'];?> </title>
       <meta charset="UTF-8">
       <meta name="description" content="<?php echo $conf['tagline'];?>">
       <meta name="keywords" content="ZWI, publish, articles, blogs">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="index,follow">
       <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
       <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
       <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
       <link rel="manifest" href="favicon/site.webmanifest">
       <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
       <meta name="msapplication-TileColor" content="#da532c">
       <meta name="theme-color" content="#ffffff">
       <link REL="SHORTCUT ICON" HREF="favicon/favicon.ico">

       <meta name="author" content="S.Chekanov">
       <link rel="stylesheet" type="text/css" href="css/datatables.css"/>
     	<style type="text/css" title="currentStyle">
                 @import "css/demo.css";
	</style>

                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <link rel="stylesheet" type="text/css" href="css/style.css"/>
		<script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>
                <script type="text/javascript" src="js/datatables.min.js"></script>

                <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
                <script src="bootstrap/js/bootstrap.bundle.min.js"></script>


		<!-- Start here -->
		<?php 
		 include "serverdatapdo.php";   // this class handles is both server and client side data
		 
		 //now generate the datatables Jquery with this SQL here:
		 //for added security move this array into to the serverside client
		 $db_array=array( 
				"sql"=>'SELECT rowid,title,publisher,path,hash FROM dbase_zwi', /* Spell out columns names no SELECT * Table */
				"table"=>'dbase_zwi', /* DB table to use assigned by constructor*/
				"idxcol"=>'rowid' /* Indexed column (used for fast and accurate table cardinality) */
					);
		$javascript = ServerDataPDO::build_jquery_datatable($db_array);
		echo $javascript;
		?>		


      
<style>
body {
        background: url('images/earth2.png') no-repeat center center fixed;
        background-size: 100% 100%;
        background-position: center;
        background-position-y: 0px;
}

pre {
  font-size: 1em;
  border: 2px solid grey;
  border-left: 10px solid #6495ED;
  border-radius: 4px;
  padding: 4px;
  display: inline-block;
  background-color: #EBECE4; 
  text-align: left;
  white-space: pre-line;
}

.navbar-brand {
  color: #85c1e9;
}

.btn:hover {
  color: var(--bs-btn-hover-color);
  background-color: #85c1e9;
  border-color: #0077b8;
}

</style>


    </head>
    <body>



<script>
$(document).ready(function() {
    var dataTable = $('#datatable1').dataTable();
    $("#searchbox").keyup(function() {
        dataTable.fnFilter(this.value);
    });    
});
</script>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container-fluid">
    <a class="navbar-brand" href="./"> <?php echo $conf['title'];  ?> </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"  data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height:520px;">
        <li class="nav-item">
          <a class="nav-link" href="articles/">Articles</a>
        </li>
       <li class="nav-item">
          <a class="nav-link" href="editor/">+Add</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="edit/">Edit</a>
        </li>
	<li class="nav-item">
          <a class="nav-link" href="about/">About</a>
        </li>
      </ul>
    </div>
  </div>
  <!-- Navbar content -->
</nav>


<div class="container">

                <center>

      <H2>
      <?php echo $conf['title']; ?> 
      </H2>

      <div style="margin-bottom:20px; font-size:1.2em;">
     <?php echo $conf['tagline']; ?>
     </div> 


<?php  
	  

$text = <<<EOT
      <form class="d-flex" role="search" action="find.php" method="get">
        <input class="form-control me-2" type="search" id="searchbox"  name="query" placeholder="Search in Encyclosphere. Start typing ..." aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
EOT;
	

$init ="";
if (isset($_GET['e']))  $init =$_GET['e'];
$init=trim($init);
if ($init == "ini") {
      echo "Initializing repository: <br>"; 
      if (!is_dir("./tmp"))
                  mkdir("./tmp", 0777, true); 
      if (!is_dir("./editor/draft"))
                  mkdir("./editor/draft", 0777, true);
      $command ="./init.sh > ./tmp/install.log 2>&1";
      $output = shell_exec($command);
      //$pos = strpos($output, "All done");
      //if ($pos === false) die("Initialization failed<br>");
      //echo "<pre>".$output."</pre>";
      //echo nl2br("<pre>".$output."</pre>");
      $output = shell_exec('grep \#\# ./tmp/install.log');
      echo "<pre>".$output."</pre>";
      echo "<h3>The initialization is  <font color=\"green\">OK</font></h3>";
      echo "<form action=\"index.php\" method=\"post\" > <button type='submit'>Click to finish installation</button> </form>";
      die();
}


	
if(is_dir('./ZWI')) { 
                echo $text;
        	$cols=",Title";  //Column names for datatable headings (typically same as sql)
		$html =ServerDataPDO::build_html_datatable($cols);
		echo  $html;
      } else {

      echo "<h3>Your " . $conf['title'] ." is installed. Checking:</h3><p></p>";

      if ( !is_writable( ROOTPATH )) {
               die("<span class=\"error\">" . ROOTPATH . " is not writable by the server. <br> Set the correct group and ownership of this directory. <br> Run this (or similar) commands on Ubuntu: <br><b>sudo chgrp www-data  zwinode</b> </br>if www-data is the group of the Apache web server. <br>You may also assign Apache owner to this directory. </span><br>"); 
      } else {
          if (!is_dir("./tmp"))
                  mkdir("./tmp", 0777, true);
      } 

      // first time?            
      //  if (checkCookies() == false) die("<span class=\"error\">Cookies are disabled in this browser. Please enable cookies. </span><br>");
      if ($conf['password'] == "zwinode") die("<span class=\"error\">You did not change the master password for administrative tasks. Please edit <b>config.php</b> file and change the password.</span><br>");
      //if(extension_loaded('gd') == false ) die("Please install the <b>php_gd</b> extension<br>");
      if(extension_loaded('pdo') == false ) die("<span class=\"error\">Please install the <b>php-pdo</b> extension<p></p> Tip: Run this command on Ubuntu and restart Apache server:<br><pre>sudo apt install php-common; sudo service apache2 restart</pre></span><br>");
      if(extension_loaded('sqlite3') == false ) die("<span class=\"error\">Please install the <b>php-sqlite3</b> extension<p></p> Tip: Run this command on Ubuntu and restart Apache server: <br> <pre>sudo apt install php-sqlite3; sudo service apache2 restart</pre></span><br>");
      if(extension_loaded('apcu') == false ) die("<span class=\"error\">Please install the <b>php-apcu</b>extension<p></p> Tip: Run this command on Ubuntu and restart Apache server:<br> <pre>sudo apt install php-apcu; sudo service apache2 restart</pre></span><br>");
      if(extension_loaded('zip') == false ) die("<span class=\"error\">Please install the <b>php-zip</b>extension<p></p> Tip: Run this command on Ubuntu and restart Apache server:<br> <pre>sudo apt install php-zip; sudo service apache2 restart</pre></span><br>");
      // bash
      $command = escapeshellcmd('bash --version');
      $output = shell_exec($command);
      $pos = strpos($output, "version");
      if ($pos === false) die("Please install <b>bash</b><br>");
      // python
      $command = escapeshellcmd('python3 --version');
      $output = shell_exec($command);
      $pos = strpos($output, "3.");
      if ($pos === false) die("Please install <b>python3</b><br>");  
      // checking git
      $command = escapeshellcmd('git --version');
      $output = shell_exec($command);
      $pos = strpos($output, "version");
      if ($pos === false) die("Please install <b>git</b><br>");


      echo "<h4>The environment is <font color=\"green\">OK</font></h4><p></p>\n";  
      echo "<form action=\"index.php?e=ini\" method=\"post\" > <button type='submit'>Click to initialize the repository</button> </form>"; 
      //echo "<a href=\"index.php?e=ini\">Click to initialize repository</a>"; 
      };
?>

                 </center>

</div>


    </body>
</html>
